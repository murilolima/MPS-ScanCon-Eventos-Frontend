import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TipoAcomodacaoListComponent } from './tipo-acomodacao-list/tipoacomodacao-list.component';
import { TipoAcomodacaoListResolver } from './tipo-acomodacao-list.resolver';
import { TipoAcomodacaoCreateComponent } from './tipo-acomodacao-create/tipoacomodacao-create.component';
import { TipoAcomodacaoEditComponent } from './tipo-acomodacao-edit/tipoacomodacao-edit.component';
import { TipoAcomodacaoEditResolver } from './tipo-acomodacao-edit.resolver';
import { PreventUnsavedChanges } from 'src/app/core/guards/prevent-unsaved-changed.guard';

const routes: Routes = [
    {
        path: '',
        component: TipoAcomodacaoListComponent,
        resolve: { tipoAcomodacao: TipoAcomodacaoListResolver },
        runGuardsAndResolvers: 'always',
        data: { title: 'Tipos de Acomodação' }
    },
    {
        path: 'criar',
        component: TipoAcomodacaoCreateComponent,
        data: { title: 'Novo Tipo de Acomocação' }
    },
    {
        path: ':id/edit',
        component: TipoAcomodacaoEditComponent,
        resolve: { tipoAcomodacao: TipoAcomodacaoEditResolver },
        canDeactivate: ['TipoAcomodacaoEditComponent'],
        data: { title: 'Alterar Tipo de Acomocação' }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [
        { provide: 'TipoAcomodacaoEditComponent', useFactory: () => new PreventUnsavedChanges<TipoAcomodacaoEditComponent>() }
    ]
})
export class TipoAcomodacaoRoutingModule { }
