import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoAcomodacao } from 'src/app/core/models/parametros/tipo-acomodacao.model';
import { TipoAcomodacaoService } from '../tipo-acomodacao.service';

@Component({
    selector: 'app-acomodacao-create',
    templateUrl: './tipoacomodacao-create.component.html'
})
export class TipoAcomodacaoCreateComponent implements OnInit {
    registroAcomodacao: FormGroup;
    acomodacao = new TipoAcomodacao();

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.registroAcomodacao.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private acomodacaoService: TipoAcomodacaoService,
        private alerts: AlertsService,
        private router: Router,
        private rxFB: RxFormBuilder
    ) {}

    ngOnInit() {
        this.registroAcomodacao = this.rxFB.formGroup(this.acomodacao);
    }

    cadastraAcomodacao() {
        if (this.registroAcomodacao.invalid) {
            return;
        }
        if (this.registroAcomodacao.valid) {
            this.acomodacao = Object.assign({}, this.registroAcomodacao.value);
            this.acomodacaoService.createAcomodacao(this.acomodacao).subscribe(
                () => {
                    this.alerts.success('Tipo Acomodação cadastrada com sucesso');
                    setTimeout(() => {
                        this.router.navigate(['/tipo-acomodacao']);
                    }, 1000);
                },
                error => {
                    this.alerts.error(error);
                }
            );
        }
    }

    voltar() {
        this.router.navigate(['/tipo-acomodacao']);
    }
}
