import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm, FormGroup } from '@angular/forms';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoAcomodacao } from 'src/app/core/models/parametros/tipo-acomodacao.model';

import { TipoAcomodacaoService } from '../tipo-acomodacao.service';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
    selector: 'app-acomodacao-edit',
    templateUrl: './tipoacomodacao-edit.component.html'
})
export class TipoAcomodacaoEditComponent implements OnInit {
    editForm: FormGroup;
    tipoAcomodacao: TipoAcomodacao;
    loading = false;
    titulo: string;

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.editForm.dirty) {
            $event.returnValue = true;
        }
    }
    constructor(
        private acomodacaoService: TipoAcomodacaoService,
        private route: ActivatedRoute,
        private router: Router,
        private alerts: AlertsService,
        private rxFormBuilder: RxFormBuilder
    ) { }

    ngOnInit() {
        this.loading = true;
        this.route.data.subscribe(data => {
            this.tipoAcomodacao = data.tipoAcomodacao;
            this.loading = false;
        });
        this.titulo = this.tipoAcomodacao.descTipoAcomodacao;
        this.editForm = this.rxFormBuilder.formGroup(this.tipoAcomodacao);
    }

    updateAcomodacao() {
        if (this.editForm.invalid) {
            return;
        }
        this.acomodacaoService.updateAcomodacao(this.tipoAcomodacao.id, this.tipoAcomodacao).subscribe(
            next => {
                this.alerts.success('Tipo de Acomodação alterada com sucesso');
                this.router.navigate(['/tipo-acomodacao']);
            },
            error => {
                this.alerts.error(error);
            }
        );
        this.titulo = this.tipoAcomodacao.descTipoAcomodacao;
    }

    voltar() {
        this.router.navigate(['/tipo-acomodacao']);
    }
}
