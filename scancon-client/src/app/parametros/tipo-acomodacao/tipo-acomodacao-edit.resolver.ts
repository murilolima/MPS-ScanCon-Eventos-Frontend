import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { TipoAcomodacao } from 'src/app/core/models/parametros/tipo-acomodacao.model';
import { AlertsService } from 'src/app/core/services/alerts.service';

import { TipoAcomodacaoService } from './tipo-acomodacao.service';

@Injectable()
export class TipoAcomodacaoEditResolver implements Resolve<TipoAcomodacao> {
    /**
     *
     */
    constructor(
        private router: Router,
        private tipoAcomodacaoService: TipoAcomodacaoService,
        private alerts: AlertsService
    ) { }

    resolve(route: ActivatedRouteSnapshot): Observable<TipoAcomodacao> {
        return this.tipoAcomodacaoService.getAcomodacao(route.params.id).pipe(
            catchError(error => {
                this.router.navigate(['']);
                this.alerts.warning('Não foi possível carregar acomodação: ' + route.params.id);
                return of(null);
            })
        );
    }
}
