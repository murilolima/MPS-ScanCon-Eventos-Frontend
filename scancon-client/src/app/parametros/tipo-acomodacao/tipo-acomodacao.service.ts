import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { TipoAcomodacao } from 'src/app/core/models/parametros/tipo-acomodacao.model';
import { SettingsService } from 'src/app/core/services/settings.service';
import { concatMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { JsonDeserializerFactory, JsonDeserializer } from 'src/app/core/services/json-deserializer.service';

@Injectable({
    providedIn: 'root'
})
export class TipoAcomodacaoService {
    baseUrl: string;
    deserializer: JsonDeserializer;

    constructor(
        private http: HttpClient,
        private settings: SettingsService,
        private jsonFactory: JsonDeserializerFactory) {
        this.baseUrl = this.settings.settings.apiUrl;
        this.deserializer = this.jsonFactory.construct(TipoAcomodacao);
    }

    getAllAcomodacoes() {
        return this.http.get<TipoAcomodacao[]>(
            this.baseUrl + 'parametros/tipoAcomodacao'
        );
    }

    getAcomodacao(id: string) {
        return this.http.get<TipoAcomodacao>(this.baseUrl + 'parametros/tipoAcomodacao/' + id).pipe(
            concatMap(tipoAcomodacao => {
                return of(this.deserializer.deserialize(tipoAcomodacao));
            })
        );
    }

    createAcomodacao(acomodacao: TipoAcomodacao) {
        return this.http.post(
            this.baseUrl + 'parametros/tipoAcomodacao',
            acomodacao,
            { responseType: 'text' }
        );
    }

    deleteAcomodacao(id: string) {
        return this.http.delete(
            this.baseUrl + 'parametros/tipoAcomodacao/' + id
        );
    }

    updateAcomodacao(id: string, acomodacao: TipoAcomodacao) {
        return this.http.put(
            this.baseUrl + 'parametros/tipoAcomodacao/' + id,
            acomodacao
        );
    }
}
