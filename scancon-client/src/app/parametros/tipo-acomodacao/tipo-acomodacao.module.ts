import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';

import { TipoAcomodacaoRoutingModule } from './tipo-acomodacao-routing.module';
import { TipoAcomodacaoListComponent } from './tipo-acomodacao-list/tipoacomodacao-list.component';
import { TipoAcomodacaoCreateComponent } from './tipo-acomodacao-create/tipoacomodacao-create.component';
import { TipoAcomodacaoEditComponent } from './tipo-acomodacao-edit/tipoacomodacao-edit.component';
import { TipoAcomodacaoListResolver } from './tipo-acomodacao-list.resolver';
import { TipoAcomodacaoEditResolver } from './tipo-acomodacao-edit.resolver';
import { TooltipModule, ModalModule } from 'ngx-bootstrap';
import { PreventUnsavedChanges } from 'src/app/core/guards/prevent-unsaved-changed.guard';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [
        TipoAcomodacaoListComponent,
        TipoAcomodacaoCreateComponent,
        TipoAcomodacaoEditComponent
    ],
    imports: [
        CommonModule,
        TipoAcomodacaoRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxLoadingModule,
        SharedModule,
        RxReactiveFormsModule,
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        SharedModule
    ],
    providers: [
        TipoAcomodacaoListResolver,
        TipoAcomodacaoEditResolver,
        PreventUnsavedChanges
    ]
})
export class TipoAcomodacaoModule { }
