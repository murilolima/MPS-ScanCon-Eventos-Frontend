import { Component, OnInit } from '@angular/core';

import { TipoAcomodacao } from 'src/app/core/models/parametros/tipo-acomodacao.model';
import { AlertsService } from 'src/app/core/services/alerts.service';

import { TipoAcomodacaoService } from '../tipo-acomodacao.service';

@Component({
  selector: 'app-acomodacao-list',
  templateUrl: './tipoacomodacao-list.component.html'
})
export class TipoAcomodacaoListComponent implements OnInit {

  acomodacoes: TipoAcomodacao[] = [];

  loading = false;

  constructor(private acomodacaoService: TipoAcomodacaoService,
              private alerts: AlertsService) { }

  ngOnInit() {
    this.loadAcomodacoes();
  }

  loadAcomodacoes() {
    this.loading = true;
    this.acomodacaoService.getAllAcomodacoes()
      .subscribe(acomodacoes => {
        this.loading = false;
        this.acomodacoes = acomodacoes;
      });
  }

  deleteAcomodacao(id: string) {
    this.loading = true;
    this.acomodacaoService.deleteAcomodacao(id)
      .subscribe(() => {
        this.loading = false;
        this.acomodacoes.splice(this.acomodacoes.findIndex(p => p.id === id), 1);
        this.alerts.success('Tipo de Acomodação excluída com sucesso');
      }, error => {
        this.loading = false;
        this.alerts.error(error);
      });
  }
}
