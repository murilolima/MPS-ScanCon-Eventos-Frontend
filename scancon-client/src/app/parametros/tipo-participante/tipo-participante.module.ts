import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { TooltipModule, ModalModule } from 'ngx-bootstrap';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';

import { TipoParticipanteRoutingModule } from './tipo-participante-routing.module';
import { TipoParticipanteListComponent } from './tipo-participante-list/tipo-participante-list.component';
import { TipoParticipanteCreateComponent } from './tipo-participante-create/tipo-participante-create.component';
import { TipoParticipanteEditComponent } from './tipo-participante-edit/tipo-participante-edit.component';
import { TipoParticipanteListResolver } from './tipo-participante-list.resolver';
import { TipoParticipanteEditResolver } from './tipo-participante-edit.resolver';
import { PreventUnsavedChanges } from 'src/app/core/guards/prevent-unsaved-changed.guard';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [
        TipoParticipanteListComponent,
        TipoParticipanteCreateComponent,
        TipoParticipanteEditComponent
    ],
    imports: [
        CommonModule,
        TipoParticipanteRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxLoadingModule,
        SharedModule,
        RxReactiveFormsModule,
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        SharedModule
    ],
    providers: [
        TipoParticipanteListResolver,
        TipoParticipanteEditResolver,
        PreventUnsavedChanges
    ]
})
export class TipoParticipanteModule { }
