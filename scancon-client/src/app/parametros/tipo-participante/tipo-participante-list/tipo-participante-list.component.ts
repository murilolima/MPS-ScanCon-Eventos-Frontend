import { Component, OnInit, Input } from '@angular/core';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoParticipante } from 'src/app/core/models/parametros/tipo-participante.model';

import { TipoParticipanteService } from '../tipo-participante.service';

@Component({
    selector: 'app-tipoparticipante-list',
    templateUrl: './tipo-participante-list.component.html'
})
export class TipoParticipanteListComponent implements OnInit {

    @Input() tipo: TipoParticipante;
    tipoParticipantes: TipoParticipante[] = [];
    qtde: any;
    loading = false;

    constructor(private tipoParticipanteService: TipoParticipanteService, private alert: AlertsService) { }

    ngOnInit() {
        this.loadTipoParticipantes();
    }

    loadTipoParticipantes() {
        this.loading = true;
        this.tipoParticipanteService.getAllTipoParticipantes().subscribe(tipoParticipantes => {
            this.loading = false;
            this.tipoParticipantes = tipoParticipantes;
        });
    }

    deleteTipoParticipante(tipoId: string) {
        this.loading = true;
        this.tipoParticipanteService.deleteTipoParticipante(tipoId)
            .subscribe(() => {
                this.loading = false;
                this.tipoParticipantes.splice(this.tipoParticipantes.findIndex(p => p.id === tipoId), 1);
                this.alert.success('Tipo de Participante excluído com sucesso');
            }, error => {
                this.loading = false;
                this.alert.error(error);
            });
    }
}
