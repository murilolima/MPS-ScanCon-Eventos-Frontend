import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { TipoParticipante } from 'src/app/core/models/parametros/tipo-participante.model';
import { TipoParticipanteService } from './tipo-participante.service';

import { AlertsService } from 'src/app/core/services/alerts.service';

@Injectable()
export class TipoParticipanteEditResolver implements Resolve<TipoParticipante> {
    /**
     *
     */
    constructor(
        private router: Router,
        private tipoParticipanteService: TipoParticipanteService,
        private alerts: AlertsService
    ) { }

    resolve(route: ActivatedRouteSnapshot): Observable<TipoParticipante> {
        return this.tipoParticipanteService.getTipoParticipante(route.params.id).pipe(
            catchError(error => {
                this.alerts.warning('Não foi possível carregar participante: ' + route.params.id);
                this.router.navigate(['']);
                return of(null);
            })
        );
    }
}
