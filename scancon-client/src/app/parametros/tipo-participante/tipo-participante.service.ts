import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { TipoParticipante } from 'src/app/core/models/parametros/tipo-participante.model';
import { SettingsService } from 'src/app/core/services/settings.service';
import { JsonDeserializer, JsonDeserializerFactory } from 'src/app/core/services/json-deserializer.service';
import { concatMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TipoParticipanteService {
    baseUrl: string;
    deserializer: JsonDeserializer;

    constructor(
        private http: HttpClient,
        private settings: SettingsService,
        private jsonFactory: JsonDeserializerFactory) {
        this.baseUrl = this.settings.settings.apiUrl;
        this.deserializer = this.jsonFactory.construct(TipoParticipante);
    }

    getAllTipoParticipantes() {
        return this.http.get<TipoParticipante[]>(this.baseUrl + 'parametros/tipoParticipante');
    }

    getTipoParticipante(tipoId: string) {
        return this.http.get<TipoParticipante>(this.baseUrl + 'parametros/tipoParticipante/' + tipoId).pipe(
            concatMap(tipoParticipante => {
                return of(this.deserializer.deserialize(tipoParticipante));
            })
        );
    }

    createTipoParticipante(tipoParticipante: TipoParticipante) {
        return this.http.post(this.baseUrl + 'parametros/tipoParticipante', tipoParticipante, { responseType: 'text' });
    }

    deleteTipoParticipante(tipoParticipanteId: string) {
        return this.http.delete(this.baseUrl + 'parametros/tipoParticipante/' + tipoParticipanteId);
    }

    updateTipoParticipante(tipoId: string, tipoParticipante: TipoParticipante) {
        return this.http.put(this.baseUrl + 'parametros/tipoParticipante/' + tipoId, tipoParticipante);
    }
}
