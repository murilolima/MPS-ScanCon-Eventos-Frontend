import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoParticipante } from 'src/app/core/models/parametros/tipo-participante.model';

import { TipoParticipanteService } from '../tipo-participante.service';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
    selector: 'app-tipoparticipante-edit',
    templateUrl: './tipo-participante-edit.component.html'
})
export class TipoParticipanteEditComponent implements OnInit {
    editForm: FormGroup;
    tipoParticipante: TipoParticipante;
    loading = false;
    titulo: string;

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.editForm.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private tipoParticipanteService: TipoParticipanteService,
        private route: ActivatedRoute,
        private router: Router,
        private alerts: AlertsService,
        private rxFormBuilder: RxFormBuilder
    ) { }

    ngOnInit() {
        this.loading = true;
        this.route.data.subscribe(data => {
            this.loading = false;
            this.tipoParticipante = data.tipoParticipante;
        });
        this.titulo = this.tipoParticipante.descTipoParticipante;
        this.editForm = this.rxFormBuilder.formGroup(this.tipoParticipante);
    }

    updateTipoParticipante() {
        if (this.editForm.invalid) {
            return;
        }
        this.tipoParticipanteService.updateTipoParticipante(this.tipoParticipante.id, this.tipoParticipante).subscribe(
            () => {
                this.alerts.success('Editado com sucesso');
                this.editForm.reset(this.tipoParticipante);
                this.voltar();
            },
            error => {
                this.alerts.error(error);
            }
        );
        this.titulo = this.tipoParticipante.descTipoParticipante;
    }

    voltar() {
        this.router.navigate(['/tipo-participante']);
    }
}
