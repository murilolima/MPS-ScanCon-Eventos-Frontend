import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoParticipante } from 'src/app/core/models/parametros/tipo-participante.model';
import { TipoParticipanteService } from '../tipo-participante.service';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
    selector: 'app-tipoparticipante-create',
    templateUrl: './tipo-participante-create.component.html'
})
export class TipoParticipanteCreateComponent implements OnInit {
    registroTipoParticipante: FormGroup;
    tipoParticipante = new TipoParticipante();

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.registroTipoParticipante.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private tipoParticipanteService: TipoParticipanteService,
        private fb: FormBuilder,
        private router: Router,
        private alert: AlertsService,
        private rxFB: RxFormBuilder
    ) { }

    ngOnInit() {
        this.registroTipoParticipante = this.rxFB.formGroup(this.tipoParticipante);
    }

    cadastrarTipoParticipante() {
        if (this.registroTipoParticipante.invalid) {
            return;
        }
        if (this.registroTipoParticipante.valid) {
            this.tipoParticipante = Object.assign({}, this.registroTipoParticipante.value);
            this.tipoParticipanteService.createTipoParticipante(this.tipoParticipante).subscribe(
                () => {
                    this.alert.success('Tipo Participante cadastrado com sucesso');
                    setTimeout(() => {
                        this.router.navigate(['/tipo-participante']);
                    }, 1000);
                },
                error => {
                    this.alert.error(error);
                }
            );
        }
    }

    voltar() {
        this.router.navigate(['/tipo-participante']);
    }
}
