import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TipoParticipanteEditResolver } from './tipo-participante-edit.resolver';
import { TipoParticipanteListComponent } from './tipo-participante-list/tipo-participante-list.component';
import { TipoParticipanteListResolver } from './tipo-participante-list.resolver';
import { TipoParticipanteCreateComponent } from './tipo-participante-create/tipo-participante-create.component';
import { TipoParticipanteEditComponent } from './tipo-participante-edit/tipo-participante-edit.component';
import { PreventUnsavedChanges } from 'src/app/core/guards/prevent-unsaved-changed.guard';

const routes: Routes = [
    {
        path: '',
        component: TipoParticipanteListComponent,
        resolve: { tipoParticipante: TipoParticipanteListResolver },
        runGuardsAndResolvers: 'always',
        data: { title: 'Tipos de Participante' }
    },
    {
        path: 'criar',
        component: TipoParticipanteCreateComponent,
        data: { title: 'Novo Tipo de Participante' }
    },
    {
        path: ':id/edit',
        component: TipoParticipanteEditComponent,
        resolve: { tipoParticipante: TipoParticipanteEditResolver },
        canDeactivate: ['TipoParticipanteEditComponent'],
        data: { title: 'Alterar Tipo de Participante' }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [
        { provide: 'TipoParticipanteEditComponent', useFactory: () => new PreventUnsavedChanges<TipoParticipanteEditComponent>() }
    ]
})
export class TipoParticipanteRoutingModule { }
