import { Component, OnInit, Input } from '@angular/core';
import { AlertsService } from 'src/app/core/services/alerts.service';

import { TipoComprovante } from 'src/app/core/models/parametros/tipo-comprovante.model';

import { TipoComprovanteService } from '../tipo-comprovante.service';

@Component({
    selector: 'app-comprovante-list',
    templateUrl: './tipocomprovante-list.component.html'
})
export class TipoComprovanteListComponent implements OnInit {
    @Input() comprovante: TipoComprovante;
    comprovantes: TipoComprovante[] = [];
    loading = false;

    constructor(private comprovanteService: TipoComprovanteService, private alerts: AlertsService) {}

    ngOnInit() {
        this.loadComprovantes();
    }

    loadComprovantes() {
        this.loading = true;
        this.comprovanteService.getAllComprovantes().subscribe(comprovantes => {
            this.loading = false;
            this.comprovantes = comprovantes;
        });
    }

    deleteComprovante(comprovanteId: string) {
        this.loading = true;
        this.comprovanteService.deleteComprovante(comprovanteId).then(
            () => {
                this.loading = false;
                this.comprovantes.splice(this.comprovantes.findIndex(p => p.id === comprovanteId), 1);
                this.alerts.success('Tipo de Comprovante excluído com sucesso');
            },
            error => {
                this.loading = false;
                this.alerts.error(error);
            }
        );
    }
}
