import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { TipoComprovante } from 'src/app/core/models/parametros/tipo-comprovante.model';
import { AlertsService } from 'src/app/core/services/alerts.service';

import { TipoComprovanteService } from './tipo-comprovante.service';

@Injectable()
export class TipoComprovanteEditResolver implements Resolve<TipoComprovante> {
    /**
     *
     */
    constructor(
        private router: Router,
        private comprovanteService: TipoComprovanteService,
        private alerts: AlertsService
    ) { }

    resolve(route: ActivatedRouteSnapshot): Observable<TipoComprovante> {
        return this.comprovanteService.getComprovante(route.params.id).pipe(
            catchError(error => {
                this.router.navigate(['']);
                this.alerts.warning('Não foi possível carregar comprovante: ' + route.params.id);
                return of(null);
            })
        );
    }
}
