import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoComprovante } from 'src/app/core/models/parametros/tipo-comprovante.model';

import { TipoComprovanteService } from '../tipo-comprovante.service';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
    selector: 'app-comprovante-edit',
    templateUrl: './tipocomprovante-edit.component.html'
})
export class TipoComprovanteEditComponent implements OnInit {
    editForm: FormGroup;
    tipoComprovante: TipoComprovante;
    loading = false;
    titulo: string;

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.editForm.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private comprovanteService: TipoComprovanteService,
        private route: ActivatedRoute,
        private router: Router,
        private alerts: AlertsService,
        private rxFB: RxFormBuilder
    ) {}

    ngOnInit() {
        this.loading = true;
        this.route.data.subscribe(data => {
            this.loading = false;
            this.tipoComprovante = data.tipoComprovante;
        });
        this.titulo = this.tipoComprovante.codTipoComprovante;
        this.editForm = this.rxFB.formGroup(this.tipoComprovante);
    }

    updateComprovante() {
        if (this.editForm.invalid) {
            return;
        }

        this.comprovanteService.updateComprovante(this.tipoComprovante.id, this.tipoComprovante).then(
            next => {
                this.alerts.success('Tipo Comprovante alterado com sucesso');
                this.editForm.reset(this.tipoComprovante);
                this.voltar();
            },
            error => {
                this.alerts.error(error);
            }
        );
        this.titulo = this.tipoComprovante.codTipoComprovante;
    }

    voltar() {
        this.router.navigate(['/tipo-comprovante']);
    }
}
