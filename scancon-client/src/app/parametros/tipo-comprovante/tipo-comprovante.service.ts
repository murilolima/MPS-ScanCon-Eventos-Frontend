import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { TipoComprovante } from 'src/app/core/models/parametros/tipo-comprovante.model';
import { SettingsService } from 'src/app/core/services/settings.service';
import { JsonDeserializer, JsonDeserializerFactory } from 'src/app/core/services/json-deserializer.service';
import { concatMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TipoComprovanteService {
    baseUrl: string;
    deserializer: JsonDeserializer;

    constructor(
        private http: HttpClient,
        private settings: SettingsService,
        private jsonFactory: JsonDeserializerFactory) {
        this.baseUrl = this.settings.settings.apiUrl;
        this.deserializer = this.jsonFactory.construct(TipoComprovante);
    }

    getAllComprovantes() {
        return this.http.get<TipoComprovante[]>(this.baseUrl + 'parametros/tipoComprovante');
    }

    getComprovante(id: string) {
        return this.http.get<TipoComprovante>(this.baseUrl + 'parametros/tipoComprovante/' + id).pipe(
            concatMap(tipoComprovante => {
                return of(this.deserializer.deserialize(tipoComprovante));
            })
        );
    }

    async deleteComprovante(id: string) {
        return await this.http.delete(this.baseUrl + 'parametros/tipoComprovante/' + id).toPromise();
    }

    async updateComprovante(id: string, comprovante: TipoComprovante) {
        return await this.http.put(this.baseUrl + 'parametros/tipoComprovante/' + id, comprovante).toPromise();
    }

    async createComprovante(comprovante: TipoComprovante) {
        return await this.http.post(this.baseUrl + 'parametros/tipoComprovante', comprovante, { responseType: 'text' }).toPromise();
    }
}
