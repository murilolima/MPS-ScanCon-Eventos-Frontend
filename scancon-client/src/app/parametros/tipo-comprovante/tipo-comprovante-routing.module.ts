import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TipoComprovanteEditComponent } from './tipo-comprovante-edit/tipocomprovante-edit.component';
import { TipoComprovanteListComponent } from './tipo-comprovante-list/tipocomprovante-list.component';
import { TipoComprovanteListResolver } from './tipo-comprovante-list.resolver';
import { TipoComprovanteCreateComponent } from './tipo-comprovante-create/tipocomprovante-create.component';
import { TipoComprovanteEditResolver } from './tipo-comprovante-edit.resolver';
import { PreventUnsavedChanges } from 'src/app/core/guards/prevent-unsaved-changed.guard';

const routes: Routes = [
    {
        path: '',
        component: TipoComprovanteListComponent,
        resolve: { tipoComprovante: TipoComprovanteListResolver },
        runGuardsAndResolvers: 'always',
        data: { title: 'Tipos de Comprovante' }
    },
    {
        path: 'criar',
        component: TipoComprovanteCreateComponent,
        data: { title: 'Novo Tipo de Comprovante' }
    },
    {
        path: ':id/edit',
        component: TipoComprovanteEditComponent,
        resolve: { tipoComprovante: TipoComprovanteEditResolver },
        canDeactivate: ['TipoComprovanteEditComponent'],
        data: { title: 'Alterar Tipo de Comprovante' }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [
        {provide: 'TipoComprovanteEditComponent', useFactory: () => new PreventUnsavedChanges<TipoComprovanteEditComponent>()}
    ]
})
export class TipoComprovanteRoutingModule { }
