import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoComprovante } from 'src/app/core/models/parametros/tipo-comprovante.model';
import { TipoComprovanteService } from '../tipo-comprovante.service';

@Component({
    selector: 'app-comprovante-create',
    templateUrl: './tipocomprovante-create.component.html'
})
export class TipoComprovanteCreateComponent implements OnInit {
    registroComprovante: FormGroup;
    comprovante = new TipoComprovante();

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.registroComprovante.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private comprovanteService: TipoComprovanteService,
        private router: Router,
        private alerts: AlertsService,
        private rxFB: RxFormBuilder
    ) {}

    ngOnInit() {
        this.registroComprovante = this.rxFB.formGroup(this.comprovante);
    }

    cadastraComprovante() {
        if (this.registroComprovante.invalid) {
            return;
        }

        if (this.registroComprovante.valid) {
            this.comprovante = Object.assign({}, this.registroComprovante.value);
            this.comprovanteService.createComprovante(this.comprovante).then(
                () => {
                    this.alerts.success('Tipo Comprovante criado com sucesso');
                    setTimeout(() => {
                        this.router.navigate(['/tipo-comprovante']);
                    }, 1000);
                },
                error => {
                    this.alerts.error(error);
                }
            );
        }
    }

    voltar() {
        this.router.navigate(['/tipo-comprovante']);
    }
}
