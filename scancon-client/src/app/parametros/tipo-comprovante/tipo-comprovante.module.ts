import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxLoadingModule } from 'ngx-loading';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TooltipModule, ModalModule } from 'ngx-bootstrap';

import { TipoComprovanteRoutingModule } from './tipo-comprovante-routing.module';
import { TipoComprovanteListComponent } from './tipo-comprovante-list/tipocomprovante-list.component';
import { TipoComprovanteCreateComponent } from './tipo-comprovante-create/tipocomprovante-create.component';
import { TipoComprovanteEditComponent } from './tipo-comprovante-edit/tipocomprovante-edit.component';
import { TipoComprovanteListResolver } from './tipo-comprovante-list.resolver';
import { TipoComprovanteEditResolver } from './tipo-comprovante-edit.resolver';
import { PreventUnsavedChanges } from 'src/app/core/guards/prevent-unsaved-changed.guard';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [
        TipoComprovanteListComponent,
        TipoComprovanteCreateComponent,
        TipoComprovanteEditComponent
    ],
    imports: [
        CommonModule,
        TipoComprovanteRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        NgxLoadingModule,
        RxReactiveFormsModule,
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        SharedModule
    ],
    providers: [
        TipoComprovanteListResolver,
        TipoComprovanteEditResolver,
        PreventUnsavedChanges
    ]
})
export class TipoComprovanteModule { }
