import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoComprovante } from 'src/app/core/models/parametros/tipo-comprovante.model';

import { TipoComprovanteService } from './tipo-comprovante.service';

@Injectable()
export class TipoComprovanteListResolver implements Resolve<TipoComprovante[]> {
    /**
     *
     */
    constructor(
        private router: Router,
        private comprovanteService: TipoComprovanteService,
        private alerts: AlertsService
    ) { }

    resolve(route: ActivatedRouteSnapshot): Observable<TipoComprovante[]> {
        return this.comprovanteService.getAllComprovantes().pipe(
            catchError(error => {
                this.router.navigate(['']);
                this.alerts.warning('Não foi possível carregar Comprovantes');
                return of(null);
            })
        );
    }
}
