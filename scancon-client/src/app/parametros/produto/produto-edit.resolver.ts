import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AlertsService } from 'src/app/core/services/alerts.service';

import { Produto } from 'src/app/core/models/parametros/produto.model';
import { ProdutoService } from './produto.service';

@Injectable()
export class ProdutoEditResolver implements Resolve<Produto> {
    /**
     *
     */
    constructor(
        private router: Router,
        private produtoService: ProdutoService,
        private alerts: AlertsService
    ) { }

    resolve(route: ActivatedRouteSnapshot): Observable<Produto> {
        return this.produtoService.getProduto(route.params.id).pipe(
            catchError(error => {
                this.router.navigate(['']);
                this.alerts.warning('Não foi possível carregar produto: ' + route.params.id);
                return of(null);
            })
        );
    }
}
