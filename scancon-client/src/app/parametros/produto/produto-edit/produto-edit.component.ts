import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { Produto } from 'src/app/core/models/parametros/produto.model';

import { ProdutoService } from '../produto.service';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
    selector: 'app-produto-edit',
    templateUrl: './produto-edit.component.html'
})
export class ProdutoEditComponent implements OnInit {
    editForm: FormGroup;
    produto = new Produto();
    loading = false;
    titulo: string;

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.editForm.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private produtoService: ProdutoService,
        private route: ActivatedRoute,
        private router: Router,
        private alerts: AlertsService,
        private rxFormBuilder: RxFormBuilder
    ) { }

    ngOnInit() {
        this.loading = true;
        this.route.data.subscribe(data => {
            this.produto = data.produto;
            this.loading = false;
        });
        this.titulo = this.produto.codProduto;
        this.editForm = this.rxFormBuilder.formGroup(this.produto);
    }

    updateProduto() {
        if (this.editForm.invalid) {
            return;
        }

        this.produtoService.updateProduto(this.produto.id, this.produto).subscribe(
            () => {
                this.alerts.success('Produto alterado com sucesso');
                this.voltar();
            },
            error => {
                this.alerts.error(error);
            }
        );
        this.titulo = this.produto.codProduto;
    }

    voltar() {
        this.router.navigate(['/produto']);
    }
}
