import { Component, OnInit, Input } from '@angular/core';

import { Produto } from 'src/app/core/models/parametros/produto.model';
import { AlertsService } from 'src/app/core/services/alerts.service';

import { ProdutoService } from '../produto.service';

@Component({
    selector: 'app-produto-list',
    templateUrl: './produto-list.component.html'
})
export class ProdutoListComponent implements OnInit {

  @Input() prod: Produto;
  produtos: Produto[] = [];
  qtde: any;

  loading = false;

  constructor(private produtoService: ProdutoService, private alert: AlertsService) { }

  ngOnInit() {
    this.loadProdutos();
  }

  loadProdutos() {
    this.loading = true;
    this.produtoService.getAllProdutos().subscribe(produtos => {
      this.loading = false;
      this.produtos = produtos;
    });
  }

  deleteProduto(prodId: string) {
    this.loading = true;
    this.produtoService.deleteProduto(prodId)
      .subscribe(() => {
        this.loading = false;
        this.produtos.splice(this.produtos.findIndex(p => p.id === prodId), 1);
        this.alert.success('Produto excluído com sucesso');
      }, error => {
        this.loading = false;
        this.alert.error(error);
      });
  }
}
