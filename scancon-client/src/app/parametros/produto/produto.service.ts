import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SettingsService } from 'src/app/core/services/settings.service';

import { Produto } from 'src/app/core/models/parametros/produto.model';
import { of } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { JsonDeserializerFactory, JsonDeserializerService } from 'src/app/core/services/json-deserializer.service';


@Injectable({
    providedIn: 'root'
})
export class ProdutoService {
    baseUrl: string;
    deserializer: JsonDeserializerService;

    constructor(
        private http: HttpClient,
        private settings: SettingsService,
        private jsonFactory: JsonDeserializerFactory) {
        this.baseUrl = this.settings.settings.apiUrl;
        this.deserializer = this.jsonFactory.construct(Produto);
    }

    getAllProdutos() {
        return this.http.get<Produto[]>(this.baseUrl + 'parametros/produto');
    }

    getProduto(prodId: string) {
        return this.http.get<Produto>(this.baseUrl + 'parametros/produto/' + prodId).pipe(
            concatMap(produto => {
                return of(this.deserializer.deserialize(produto));
            })
        );
    }

    createProduto(produto: Produto) {
        return this.http.post(this.baseUrl + 'parametros/produto', produto, {
            responseType: 'text'
        });
    }

    deleteProduto(produtoId: string) {
        return this.http.delete(
            this.baseUrl + 'parametros/produto/' + produtoId
        );
    }

    updateProduto(prodId: string, produto: Produto) {
        return this.http.put(
            this.baseUrl + 'parametros/produto/' + prodId,
            produto
        );
    }
}
