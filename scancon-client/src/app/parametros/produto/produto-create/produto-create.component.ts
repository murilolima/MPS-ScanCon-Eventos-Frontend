import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { Produto } from 'src/app/core/models/parametros/produto.model';
import { ProdutoService } from '../produto.service';

@Component({
    selector: 'app-produto-create',
    templateUrl: './produto-create.component.html'
})
export class ProdutoCreateComponent implements OnInit {
    registroProduto: FormGroup;
    produto = new Produto();

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.registroProduto.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private produtoService: ProdutoService,
        private router: Router,
        private alert: AlertsService,
        private rxFB: RxFormBuilder
    ) { }

    ngOnInit() {
        this.registroProduto = this.rxFB.formGroup(this.produto);
    }

    cadastrarProduto() {
        if (this.registroProduto.invalid) {
            return;
        }

        if (this.registroProduto.valid) {
            this.produto = Object.assign({}, this.registroProduto.value);
            this.produtoService.createProduto(this.produto).subscribe(
                () => {
                    this.alert.success('Produto cadastrada com sucesso');
                    setTimeout(() => {
                        this.router.navigate(['/produto']);
                    }, 1000);
                },
                error => {
                    this.alert.error(error);
                }
            );
        }
    }

    voltar() {
        this.router.navigate(['/produto']);
    }
}
