import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';

import { ProdutoRoutingModule } from './produto-routing.module';
import { ProdutoEditResolver } from './produto-edit.resolver';
import { ProdutoListResolver } from './produto-list.resolver';
import { ProdutoListComponent } from './produto-list/produto-list.component';
import { ProdutoCreateComponent } from './produto-create/produto-create.component';
import { ProdutoEditComponent } from './produto-edit/produto-edit.component';
import { PreventUnsavedChanges } from 'src/app/core/guards/prevent-unsaved-changed.guard';
import { TooltipModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [
        ProdutoListComponent,
        ProdutoCreateComponent,
        ProdutoEditComponent
    ],
    imports: [
        CommonModule,
        ProdutoRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxLoadingModule,
        SharedModule,
        RxReactiveFormsModule,
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        SharedModule
    ],
    providers: [
        ProdutoListResolver,
        ProdutoEditResolver,
        PreventUnsavedChanges
    ]
})
export class ProdutoModule { }
