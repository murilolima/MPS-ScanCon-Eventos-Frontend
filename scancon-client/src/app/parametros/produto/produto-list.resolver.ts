import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { ProdutoService } from './produto.service';
import { Produto } from 'src/app/core/models/parametros/produto.model';

@Injectable()
export class ProdutoListResolver implements Resolve<Produto[]> {

    constructor(
        private router: Router,
        private produtoService: ProdutoService,
        private alerst: AlertsService
    ) { }

    resolve(route: ActivatedRouteSnapshot): Observable<Produto[]> {
        return this.produtoService.getAllProdutos().pipe(
            catchError(error => {
                this.router.navigate(['']);
                this.alerst.warning('Não foi possível carregar Produtos');
                return of(null);
            })
        );
    }
}
