import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProdutoListResolver } from './produto-list.resolver';
import { ProdutoEditResolver } from './produto-edit.resolver';
import { ProdutoListComponent } from './produto-list/produto-list.component';
import { ProdutoCreateComponent } from './produto-create/produto-create.component';
import { ProdutoEditComponent } from './produto-edit/produto-edit.component';
import { PreventUnsavedChanges } from 'src/app/core/guards/prevent-unsaved-changed.guard';

const routes: Routes = [
    {
        path: '',
        component: ProdutoListComponent,
        resolve: { produto: ProdutoListResolver },
        runGuardsAndResolvers: 'always',
        data: { title: 'Produtos' }
    },
    {
        path: 'criar',
        component: ProdutoCreateComponent,
        data: { title: 'Novo Produto' }
    },
    {
        path: ':id/edit',
        component: ProdutoEditComponent,
        resolve: { produto: ProdutoEditResolver },
        canDeactivate: ['ProdutoEditComponent'],
        data: { title: 'Alterar Produto' }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [
        { provide: 'ProdutoEditComponent', useFactory: () => new PreventUnsavedChanges<ProdutoEditComponent>() }
    ]
})
export class ProdutoRoutingModule { }
