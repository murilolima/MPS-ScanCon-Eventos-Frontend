import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { TipoEvento } from 'src/app/core/models/parametros/tipo-evento.model';
import { SettingsService } from 'src/app/core/services/settings.service';
import { JsonDeserializer, JsonDeserializerFactory } from 'src/app/core/services/json-deserializer.service';
import { concatMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TipoEventoService {
    baseUrl: string;
    deserializer: JsonDeserializer;

    constructor(
        private http: HttpClient,
        private settings: SettingsService,
        private jsonFactory: JsonDeserializerFactory) {
        this.baseUrl = this.settings.settings.apiUrl;
        this.deserializer = this.jsonFactory.construct(TipoEvento);
    }

    getAllTipoEventos() {
        return this.http.get<TipoEvento[]>(this.baseUrl + 'parametros/tipoEvento');
    }

    getTipoEvento(eventoId: string) {
        return this.http.get<TipoEvento>(this.baseUrl + 'parametros/tipoEvento/' + eventoId).pipe(
            concatMap(tipoEvento => {
                return of(this.deserializer.deserialize(tipoEvento));
            })
        );
    }

    createTipoEvento(tipoEvento: TipoEvento) {
        return this.http.post(this.baseUrl + 'parametros/tipoEvento', tipoEvento, { responseType: 'text' });
    }

    deleteTipoEvento(tipoEventoId: string) {
        return this.http.delete(this.baseUrl + 'parametros/tipoEvento/' + tipoEventoId, {responseType: 'text'});
    }

    updateTipoEvento(eventoId: string, tipoEvento: TipoEvento) {
        return this.http.put(this.baseUrl + 'parametros/tipoEvento/' + eventoId, tipoEvento);
    }
}
