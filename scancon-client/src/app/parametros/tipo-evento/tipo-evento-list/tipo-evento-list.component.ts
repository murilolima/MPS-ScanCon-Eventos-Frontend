import { Component, OnInit, Input } from '@angular/core';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoEvento } from 'src/app/core/models/parametros/tipo-evento.model';

import { TipoEventoService } from '../tipo-evento.service';

@Component({
    selector: 'app-tipoevento-list',
    templateUrl: './tipo-evento-list.component.html'
})
export class TipoEventoListComponent implements OnInit {

    @Input() tipoEvento: TipoEvento;
    tipoEventos: TipoEvento[] = [];
    qtde: any;
    loading = false;

    constructor(private tipoEventoService: TipoEventoService, private alert: AlertsService) { }

    ngOnInit() {
        this.loadTipoEventos();
    }

    loadTipoEventos() {
        this.loading = true;
        this.tipoEventoService.getAllTipoEventos().subscribe(tipoEventos => {
            this.loading = false;
            this.tipoEventos = tipoEventos;
        });
    }

    deleteTipoEvento(tipoId: string) {
        this.loading = true;
        this.tipoEventoService.deleteTipoEvento(tipoId)
            .subscribe(() => {
                this.loading = false;
                this.tipoEventos.splice(this.tipoEventos.findIndex(p => p.id === tipoId), 1);
                this.alert.success('O tipo de Evento foi excluído com sucesso');
            }, error => {
                this.loading = false;
                this.alert.error(error);
            });
    }

}
