import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoEvento } from 'src/app/core/models/parametros/tipo-evento.model';
import { TipoEventoService } from '../tipo-evento.service';

@Component({
    selector: 'app-tipoevento-create',
    templateUrl: './tipo-evento-create.component.html'
})
export class TipoEventoCreateComponent implements OnInit {
    registroTipoEvento: FormGroup;
    tipoEvento = new TipoEvento();

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.registroTipoEvento.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private tipoEventoService: TipoEventoService,
        private fb: FormBuilder,
        private router: Router,
        private alert: AlertsService,
        private rxFB: RxFormBuilder
    ) {}

    ngOnInit() {
        this.tipoEvento.alocGrupoCota = false;
        this.tipoEvento.alocSemGrupoCota = false;
        this.tipoEvento.cadastroObrigatorio = false;
        this.tipoEvento.confirmacaoObrigatoria = false;
        this.tipoEvento.verificaCredito = false;
        this.tipoEvento.verificaInadimplencia = false;
        this.registroTipoEvento = this.rxFB.formGroup(this.tipoEvento);
    }

    cadastrarTipoEvento() {
        if (this.registroTipoEvento.invalid) {
            return;
        }

        if (this.registroTipoEvento.valid) {
            this.tipoEvento = Object.assign({}, this.registroTipoEvento.value);
            this.tipoEventoService.createTipoEvento(this.tipoEvento).subscribe(
                () => {
                    this.alert.success('Tipo Evento cadastrada com sucesso');
                    setTimeout(() => {
                        this.router.navigate(['/tipo-evento']);
                    }, 1000);
                },
                error => {
                    this.alert.error(error);
                }
            );
        }
    }

    voltar() {
        this.router.navigate(['/tipo-evento']);
    }
}
