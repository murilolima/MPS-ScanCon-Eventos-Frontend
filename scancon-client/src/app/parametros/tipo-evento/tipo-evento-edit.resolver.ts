import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { TipoEvento } from 'src/app/core/models/parametros/tipo-evento.model';
import { AlertsService } from 'src/app/core/services/alerts.service';

import { TipoEventoService } from './tipo-evento.service';

@Injectable()
export class TipoEventoEditResolver implements Resolve<TipoEvento> {
    /**
     *
     */
    constructor(
        private router: Router,
        private tipoEventoService: TipoEventoService,
        private alerts: AlertsService
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<TipoEvento> {
        return this.tipoEventoService.getTipoEvento(route.params.id).pipe(
            catchError(error => {
                this.router.navigate(['']);
                this.alerts.warning('Naõ foi possível carregar evento: ' + route.params.id);
                return of(null);
            })
        );
    }
}
