import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TipoEventoListComponent } from './tipo-evento-list/tipo-evento-list.component';
import { TipoEventoListResolver } from './tipo-evento-list.resolver';
import { TipoEventoCreateComponent } from './tipo-evento-create/tipo-evento-create.component';
import { TipoEventoEditComponent } from './tipo-evento-edit/tipo-evento-edit.component';
import { TipoEventoEditResolver } from './tipo-evento-edit.resolver';
import { PreventUnsavedChanges } from 'src/app/core/guards/prevent-unsaved-changed.guard';

const routes: Routes = [
    {
        path: '',
        component: TipoEventoListComponent,
        resolve: { tipoEvento: TipoEventoListResolver },
        runGuardsAndResolvers: 'always',
        data: { title: 'Tipos de Evento' }
    },
    {
        path: 'criar',
        component: TipoEventoCreateComponent,
        data: { title: 'Novo Tipo de Evento' }
    },
    {
        path: ':id/edit',
        component: TipoEventoEditComponent,
        resolve: { tipoEvento: TipoEventoEditResolver },
        canDeactivate: ['TipoEventoEditComponent'],
        data: { title: 'Alterar Tipo de Evento' }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [
        { provide: 'TipoEventoEditComponent', useFactory: () => new PreventUnsavedChanges<TipoEventoEditComponent>() }
    ]
})
export class TipoEventoRoutingModule { }
