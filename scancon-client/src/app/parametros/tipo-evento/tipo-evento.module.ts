import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { TooltipModule, ModalModule } from 'ngx-bootstrap';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';

import { TipoEventoRoutingModule } from './tipo-evento-routing.module';
import { TipoEventoListComponent } from './tipo-evento-list/tipo-evento-list.component';
import { TipoEventoCreateComponent } from './tipo-evento-create/tipo-evento-create.component';
import { TipoEventoEditComponent } from './tipo-evento-edit/tipo-evento-edit.component';
import { TipoEventoListResolver } from './tipo-evento-list.resolver';
import { TipoEventoEditResolver } from './tipo-evento-edit.resolver';
import { PreventUnsavedChanges } from 'src/app/core/guards/prevent-unsaved-changed.guard';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [
        TipoEventoListComponent,
        TipoEventoCreateComponent,
        TipoEventoEditComponent
    ],
    imports: [
        CommonModule,
        TipoEventoRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        NgxLoadingModule,
        RxReactiveFormsModule,
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        SharedModule
    ],
    providers: [
        TipoEventoListResolver,
        TipoEventoEditResolver,
        PreventUnsavedChanges
    ]
})
export class TipoEventoModule { }
