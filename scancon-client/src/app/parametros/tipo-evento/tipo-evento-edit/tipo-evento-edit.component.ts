import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoEvento } from 'src/app/core/models/parametros/tipo-evento.model';

import { TipoEventoService } from '../tipo-evento.service';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

@Component({
    selector: 'app-tipoevento-edit',
    templateUrl: './tipo-evento-edit.component.html'
})
export class TipoEventoEditComponent implements OnInit {
    editForm: FormGroup;
    tipoEvento: TipoEvento;
    loading = false;
    titulo: string;

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.editForm.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private tipoEventoService: TipoEventoService,
        private route: ActivatedRoute,
        private router: Router,
        private alerts: AlertsService,
        private rxFormBuilder: RxFormBuilder
    ) { }

    ngOnInit() {
        this.loading = true;
        this.route.data.subscribe(data => {
            this.loading = false;
            this.tipoEvento = data.tipoEvento;
        });
        this.titulo = this.tipoEvento.codTipoEvento;
        this.editForm = this.rxFormBuilder.formGroup(this.tipoEvento);
    }

    updateTipoEvento() {
        if (this.editForm.invalid) {
            return;
        }
        this.tipoEventoService.updateTipoEvento(this.tipoEvento.id, this.tipoEvento).subscribe(
            next => {
                this.alerts.success('Editado com sucesso');
                this.editForm.reset(this.tipoEvento);
                this.voltar();
            },
            error => {
                this.alerts.error(error);
            }
        );
        this.titulo = this.tipoEvento.codTipoEvento;
    }

    voltar() {
        this.router.navigate(['/tipo-evento']);
    }
}
