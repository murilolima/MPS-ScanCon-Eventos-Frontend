import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { TipoEvento } from 'src/app/core/models/parametros/tipo-evento.model';
import { AlertsService } from 'src/app/core/services/alerts.service';

import { TipoEventoService } from './tipo-evento.service';

@Injectable()
export class TipoEventoListResolver implements Resolve<TipoEvento[]> {
    /**
     *
     */
    constructor(
        private router: Router,
        private tipoEventoService: TipoEventoService,
        private alerts: AlertsService
    ) { }

    resolve(route: ActivatedRouteSnapshot): Observable<TipoEvento[]> {
        return this.tipoEventoService.getAllTipoEventos().pipe(
            catchError(error => {
                this.router.navigate(['']);
                this.alerts.warning('Não foi possível carregar Eventos');
                return of(null);
            })
        );
    }
}
