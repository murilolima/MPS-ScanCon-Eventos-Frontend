import { Routes } from '@angular/router';
import { HomeComponent } from './layout/home/home.component';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { HealthcheckComponent } from './layout/healthcheck/healthcheck.component';

export const appRoutes: Routes = [
    {
        path: 'health',
        component: HealthcheckComponent
    },
    {
        path: '',
        component: MainLayoutComponent,
        children: [
            {
                path: '',
                component: HomeComponent,
                data: { title: 'Início' }
            },
        ]
    },
    {
        path: '',
        runGuardsAndResolvers: 'always',
        children: [
            {
                path: 'produto',
                component: MainLayoutComponent,
                loadChildren: './parametros/produto/produto.module#ProdutoModule'
            },
            {
                path: 'tipo-acomodacao',
                component: MainLayoutComponent,
                loadChildren: './parametros/tipo-acomodacao/tipo-acomodacao.module#TipoAcomodacaoModule'
            },
            {
                path: 'tipo-comprovante',
                component: MainLayoutComponent,
                loadChildren: './parametros/tipo-comprovante/tipo-comprovante.module#TipoComprovanteModule'
            },
            {
                path: 'tipo-evento',
                component: MainLayoutComponent,
                loadChildren: './parametros/tipo-evento/tipo-evento.module#TipoEventoModule'
            },
            {
                path: 'tipo-participante',
                component: MainLayoutComponent,
                loadChildren: './parametros/tipo-participante/tipo-participante.module#TipoParticipanteModule'
            },
            {
                path: 'eventos',
                component: MainLayoutComponent,
                loadChildren: './eventos/eventos.module#EventosModule'
            },
            {
                path: 'alocacoes',
                component: MainLayoutComponent,
                loadChildren: './alocacoes/alocacoes.module#AlocacoesModule'
            },
            {
                path: 'participantes',
                component: MainLayoutComponent,
                loadChildren: './participantes/participantes.module#ParticipantesModule'
            }
        ]
    },
    { path: '**', redirectTo: '', pathMatch: 'full' }
];
