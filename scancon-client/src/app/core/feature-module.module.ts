import { NgxSettings } from './providers/ngx-settings.provider';

export class FeatureModule {
    constructor(private ngxSettings: NgxSettings) {
        this.ngxSettings.init();
    }
}
