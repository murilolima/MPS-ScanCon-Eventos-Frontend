import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({ providedIn: 'root' })
export class TitleService {

    private title: string;
    private baseTile = 'Eventos Consórcio Scania';

    public constructor(private pageTitleService: Title) { }

    getTitle() {
        return this.title;
    }

    setTitle(newTitle: string) {
        // title to be used on application
        this.title = newTitle;

        const documentTitle = `${this.baseTile} - ${newTitle}`;
        // documento title
        this.pageTitleService.setTitle(documentTitle);
    }
}
