import { Settings } from '../models/settings.model';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class SettingsService {
    private _settings: Settings;

    get settings(): Settings {
        return this._settings;
    }

    set settings(value: Settings) {
        this._settings = value;

        if (this._settings.apiUrl && !this._settings.apiUrl.endsWith('/')) {
            this._settings.apiUrl = this._settings.apiUrl + '/';
        }
    }

    constructor() {
        this.settings = new Settings();
    }
}
