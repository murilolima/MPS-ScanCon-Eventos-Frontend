import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class AlertsService {
    private options = {};

    constructor(private toatrs: ToastrService) {
        this.options = {
            positionClass: 'toast-bottom-right',
            timeOut: 4000,
            closeButton: true
        };
    }

    success(message: string) {
        this.toatrs.success(message, '', this.options);
    }

    error(message: string) {
        if (message.search('{') === 1) {
            const json = JSON.parse(message);
            const keys = Object.keys(json);
            for (const key of keys) {
                console.log(json[key]);
                this.toatrs.error(json[key], '', {
                    positionClass: 'toast-bottom-right'
                });
            }
        }
        else {
            this.toatrs.error(message, '', {
                positionClass: 'toast-bottom-right'
            });
        }
    }

    warning(message: string) {
        this.toatrs.warning(message, '', this.options);
    }

    info(message: string) {
        this.toatrs.info(message, '', this.options);
    }
}
