import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TitleService } from './services/title.service';
import { SettingsHttpProvider } from './providers/settings-http.provider';
import { ErrorInterceptorProvider } from './providers/error-interceptor.provider';
import { JsonDeserializerFactory } from './services/json-deserializer.service';
import { LocaleProvider, LocaleProviderRegister } from './providers/locale.provider';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [],
    exports: [],
    providers: [
        ErrorInterceptorProvider,
        JsonDeserializerFactory,
        SettingsHttpProvider,
        LocaleProvider,
        LocaleProviderRegister,
        TitleService
    ],
})
export class CoreModule { }
