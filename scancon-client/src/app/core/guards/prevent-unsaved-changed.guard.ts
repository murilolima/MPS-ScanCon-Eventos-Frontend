import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';

@Injectable()
export class PreventUnsavedChanges<T> implements CanDeactivate<any> {
    canDeactivate(component: any) {
        if (component.editForm.dirty) {
            return confirm('Tem certeza que deseja continuar? Todas as alterações serão perdidas');
        }
        return true;
    }
}
