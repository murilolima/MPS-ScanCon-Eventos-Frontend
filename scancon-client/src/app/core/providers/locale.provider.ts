import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import localePtExtra from '@angular/common/locales/extra/pt';
import { LOCALE_ID, APP_INITIALIZER } from '@angular/core';

export function init_locale_data() {
    return () => registerLocaleData(localePt, 'pt', localePtExtra);
}

export const LocaleProviderRegister = {
    provide: APP_INITIALIZER,
    useFactory: init_locale_data,
    multi: true
};

export const LocaleProvider = {
    provide: LOCALE_ID,
    useValue: 'pt-BR'
};
