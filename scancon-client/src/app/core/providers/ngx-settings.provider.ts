import { BsDatepickerConfig } from 'ngx-bootstrap';
import { BsLocaleService } from 'ngx-bootstrap';
import { defineLocale, ptBrLocale } from 'ngx-bootstrap/chronos';
import { Injectable } from '@angular/core';
defineLocale('pt-br', ptBrLocale);

const ngxSettingsFactory = () => {
    return Object.assign(new BsDatepickerConfig(), {
        containerClass: 'theme-blue',
        showWeekNumbers: false,
        minDate: new Date('1900-01-01')
    });
};

export const NgxSettingsProvider = {
    provide: BsDatepickerConfig,
    useFactory: ngxSettingsFactory
};

@Injectable({ providedIn: 'root' })
export class NgxSettings {
    constructor(private bsLocaleService: BsLocaleService) { }

    init() {
        this.bsLocaleService.use('pt-br');
    }
}
