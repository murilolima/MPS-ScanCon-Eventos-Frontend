import { APP_INITIALIZER } from '@angular/core';
import { SettingsHttpService } from '../services/settings-http.service';

export function init_http_settings(settingsHttpService: SettingsHttpService) {
    return () => settingsHttpService.initializeAppRemoteSettings();
}

export const SettingsHttpProvider = {
    provide: APP_INITIALIZER,
    useFactory: init_http_settings,
    deps: [SettingsHttpService],
    multi: true
};
