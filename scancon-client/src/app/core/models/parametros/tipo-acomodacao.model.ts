import { required, maxLength } from '@rxweb/reactive-form-validators';

export class TipoAcomodacao {
    id: string;

    @required()
    @maxLength({ value: 255})
    descTipoAcomodacao: string;
}
