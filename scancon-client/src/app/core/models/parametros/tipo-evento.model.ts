import { required, maxLength } from '@rxweb/reactive-form-validators';

export class TipoEvento {
    id: string;

    @required()
    codTipoEvento: string;

    @required()
    @maxLength({ value: 255})
    descTipoEvento: string;

    @required()
    alocGrupoCota?: boolean;

    @required()
    alocSemGrupoCota?: boolean;

    @required()
    verificaCredito?: boolean;

    @required()
    cadastroObrigatorio?: boolean;

    @required()
    confirmacaoObrigatoria?: boolean;

    @required()
    verificaInadimplencia?: boolean;
}
