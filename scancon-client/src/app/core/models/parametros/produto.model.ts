import { required, maxLength, minLength } from '@rxweb/reactive-form-validators';

export class Produto {
    id: string;

    @required()
    codProduto: string;

    @required()
    @maxLength({ value: 255})
    descProduto: string;

    @required()
    @maxLength({ value: 150})
    fabricante: string;

    @required()
    @maxLength({ value: 150})
    conta?: string;

    @required()
    @maxLength({ value: 150})
    contraPartida?: string;

    @required()
    @maxLength({ value: 150})
    centroCusto?: string;
}
