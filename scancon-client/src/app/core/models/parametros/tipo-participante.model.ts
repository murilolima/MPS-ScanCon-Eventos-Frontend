import { required, maxLength } from '@rxweb/reactive-form-validators';

export class TipoParticipante {
    id: string;

    @required()
    @maxLength({ value: 255})
    descTipoParticipante: string;

}
