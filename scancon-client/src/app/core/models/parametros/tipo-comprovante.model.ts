import { required, maxLength } from '@rxweb/reactive-form-validators';

export class TipoComprovante {
    id: string;

    @required()
    @maxLength({ value: 255})
    codTipoComprovante: string;

    @required()
    @maxLength({ value: 255})
    descTipoComprovante: string;

    @required()
    fatoGerador: string;

    @required()
    @maxLength({ value: 50})
    classificacao: string;
}
