import { required, maxLength, prop } from '@rxweb/reactive-form-validators';

import { Alocacao } from '../eventos/alocacao.model';
import { Participante } from './participante.model';

export class AlocacaoCotaParticipante {

    id: number;

    @required()
    @maxLength({ value: 255})
    participanteId: string;

    @prop()
    participante: Participante;

    @required()
    alocacaoId: number;

    @prop()
    alocacao: Alocacao;
}

