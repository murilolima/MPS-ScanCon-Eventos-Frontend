import { Type } from 'class-transformer';
import { required, maxLength, numeric, NumericValueType, prop, propObject, } from '@rxweb/reactive-form-validators';

import { EnderecoParticipante } from './endereco-participante.model';
import { AlocacaoCotaParticipante } from './alocacao-participante.model';

export class Participante {

    id: string;

    @required()
    @maxLength({ value: 255})
    tipoParticipanteId: string;

    @required()
    @maxLength({ value: 11})
    @numeric({ acceptValue: NumericValueType.PositiveNumber})
    cpf: string;

    @required()
    @maxLength({ value: 11})
    @numeric({ acceptValue: NumericValueType.PositiveNumber})
    rg: string;

    @required()
    sexo: string;

    @required()
    @maxLength({ value: 255})
    nomeCompleto: string;

    @required()
    @Type(() => Date)
    nascimento: Date;

    @required()
    @maxLength({ value: 255})
    email: string;

    @required()
    @maxLength({ value: 50})
    telefoneCelular: string;

    @required()
    @maxLength({ value: 50})
    telefoneResidencial: string;

    @required()
    @maxLength({ value: 50})
    telefoneComercial: string;

    @prop()
    @maxLength({ value: 50})
    passaporte: string;

    @prop()
    @Type(() => Date)
    passaporteValidade: Date;

    @prop()
    visto: string;

    @prop()
    @Type(() => Date)
    vistoValidade: Date;

    @required()
    @maxLength({ value: 255})
    localEmbarque: string;

    @required()
    @maxLength({ value: 255})
    localDesembarque: string;

    @required()
    gestante: string;

    @numeric()
    semanasGravidez: string;

    @required()
    necessidadesEspeciais: boolean;

    @prop()
    @maxLength({ value: 1000})
    observacao: string;

    @Type(() => EnderecoParticipante)
    @propObject(EnderecoParticipante)
    endereco: EnderecoParticipante;

    @prop()
    tipoParticipanteDesc: string;

    alocacaoParticipante: AlocacaoCotaParticipante[];
}
