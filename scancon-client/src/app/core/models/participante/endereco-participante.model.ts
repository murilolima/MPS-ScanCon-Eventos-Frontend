import { maxLength, required, prop } from '@rxweb/reactive-form-validators';

export class EnderecoParticipante {

    @required()
    @maxLength({ value: 255})
    logradouro: string;

    @required()
    @maxLength({ value: 255})
    bairro: string;

    @required()
    @maxLength({ value: 255})
    cidade: string;

    @required()
    @maxLength({ value: 50})
    cep: string;

    @prop()
    @maxLength({ value: 255})
    pontoReferencia: string;

    @required()
    numero: string;

    @required()
    estado: string;
}
