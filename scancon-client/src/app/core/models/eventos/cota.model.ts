import { prop, required } from '@rxweb/reactive-form-validators';

export class Cota {
    id: string;

    @required()
    concessionaria: string;

    @required()
    grupo: string;

    @required()
    codCota: string;

    @required()
    consorciado: string;

    @required()
    cpfCnpj: string;

    @required()
    credito: number;

    @required()
    adesao: Date;

    @required()
    inadimplente: boolean;

    @prop()
    alocacaoGrupocotaId: number;

    // View only property
    selected: boolean;
}
