import { required, prop } from '@rxweb/reactive-form-validators';
import { Type } from 'class-transformer';

import { Grupo } from './grupo.model';
import { Cota } from './cota.model';
import { Participante } from '../participante/participante.model';

export class Alocacao {
    id: number;

    @required()
    numAlocacao: number;

    @prop()
    grupoId: number;

    @Type(() => Grupo)
    grupo: Grupo;

    @Type(() => Cota)
    cotas: Cota[];

    @Type(() => Participante)
    participantes: Participante[];

    containsParticipante(participante: Participante) {
        return this.participantes.find(p => p.id === participante.id);
    }

    addParticipante(participante: Participante) {
        if (this.containsParticipante(participante)) {
            throw new Error('Participante já adicionado na alocação!');
        }
        this.participantes.push(participante);
    }
}
