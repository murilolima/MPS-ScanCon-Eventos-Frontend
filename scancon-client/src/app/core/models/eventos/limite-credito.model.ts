import { Evento } from './evento.model';
import { required, prop } from '@rxweb/reactive-form-validators';

export class LimiteCredito {
    id: number;

    @required()
    qtdeParticipantes: number;

    @required()
    creditMinimo: number;

    @prop()
    eventoID: number;

    evento: Evento;
}

