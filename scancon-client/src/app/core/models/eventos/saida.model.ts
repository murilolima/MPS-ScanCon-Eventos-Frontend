import { required, prop } from '@rxweb/reactive-form-validators';

import { Evento } from './evento.model';
import { Grupo } from './grupo.model';

export class Saida {
    id: number;

    @prop()
    eventoId: number;

    evento: Evento;

    @required()
    nome: string;

    @required()
    saidaEstimada: Date;

    @required()
    retornoEstimado: Date;

    @required()
    cidadeSaida: string;

    @required()
    cidadeDestino: string;

    grupos: Grupo[];


    get dataSaidaEstimada() {
        return this.saidaEstimada.toLocaleDateString();
    }
}
