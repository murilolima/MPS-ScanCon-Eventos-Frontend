import { required, prop, numeric } from '@rxweb/reactive-form-validators';
import { Type } from 'class-transformer';

import { TipoEvento } from '../parametros/tipo-evento.model';
import { Saida } from './saida.model';
import { LimiteCredito } from './limite-credito.model';

export class Evento {
    @prop()
    id: number;

    @required()
    nome: string;

    @required()
    @Type(() => Date)
    inicioVenda: Date;

    @required()
    @Type(() => Date)
    fimVenda: Date;

    @required()
    @numeric({ allowDecimal: true })
    porcentagemProvisao: number;

    @required()
    acessoRestrito: boolean = false;

    @required()
    tipoEventoId: string = '';

    @Type(() => TipoEvento)
    tipoEvento: TipoEvento;

    tipoEventoDesc: string;

    @Type(() => Saida)
    saidas: Saida[];

    limites: LimiteCredito[];
}
