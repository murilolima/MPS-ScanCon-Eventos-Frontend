import { required, prop } from '@rxweb/reactive-form-validators';

import { TipoAcomodacao } from '../parametros/tipo-acomodacao.model';
import { Saida } from './saida.model';
import { Alocacao } from '../eventos/alocacao.model';

export class Grupo {
    id: number;

    @required()
    descricao: string;

    @prop()
    tipoAcomodacaoId: string;

    tipoAcomodacao: TipoAcomodacao;

    @prop()
    saidaID: number;

    saida: Saida;

    @required()
    limiteAlocacoes: number;

    @required()
    complemento: string;

    @prop()
    voo: string;

    alocacoes: Alocacao[];

    descricaoComDataSaida() {
        return `${this.saida.dataSaidaEstimada} - ${this.descricao}`;
    }
}
