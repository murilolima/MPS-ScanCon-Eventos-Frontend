import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Evento } from '../models/eventos/evento.model';

@Injectable({ providedIn: 'root' })
export class EventosStore {

    constructor() { }

    private readonly selectedEventoSubject = new BehaviorSubject<Evento>(null);
    readonly selectedEvento$ = this.selectedEventoSubject.asObservable();
    get selectedEvento(): Evento {
        return this.selectedEventoSubject.getValue();
    }

    set selectedEvento(value: Evento) {
        this.selectedEventoSubject.next(value);
    }

    async selectEvento(evento: Evento) {
        this.selectedEvento = evento;
    }

    async unSelectEvento() {
        this.selectedEvento = null;
    }
}
