import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Cota } from '../models/eventos/cota.model';

// TODO: Mover métodos do cota service para esta classe
@Injectable({ providedIn: 'root' })
export class CotasStore {

    private readonly selectedCotasSubject = new BehaviorSubject<Cota[]>([]);
    readonly selectedCotas$ = this.selectedCotasSubject.asObservable();

    private get selectedCotas(): Cota[] {
        return this.selectedCotasSubject.getValue();
    }

    private set selectedCotas(value: Cota[]) {
        this.selectedCotasSubject.next(value);
    }

    async selectCota(cota: Cota) {
        if (this.isCotaSelected(cota)) {
            return;
        }

        cota.selected = true;
        this.selectedCotas = [
            ...this.selectedCotas,
            cota
        ];
    }

    assignCotaSelection(cota: Cota) {
        cota.selected = this.isCotaSelected(cota);
    }

    isCotaSelected(cota: Cota) {
        const selectedCota = this.selectedCotas.find(c => c.id == cota.id);
        return selectedCota != null;
    }

    async unSelectCota(cota: Cota) {
        this.selectedCotas = this.selectedCotas.filter(
            c => c.id !== cota.id
        );
        cota.selected = false;
    }

    async unselectAllCotas() {
        this.selectedCotas = [];
    }
}
