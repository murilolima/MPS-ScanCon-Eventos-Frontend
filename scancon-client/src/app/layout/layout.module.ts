import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';

import { NavComponent } from './nav/nav.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HomeComponent } from './home/home.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { HealthcheckComponent } from './healthcheck/healthcheck.component';


@NgModule({
    declarations: [
        NavComponent,
        SidebarComponent,
        HomeComponent,
        NavComponent,
        MainLayoutComponent,
        HealthcheckComponent
    ],
    imports: [
        CommonModule,
        NgbModule,
        RouterModule
    ]
})
export class LayoutModule { }
