import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    childItems?: RouteInfo[];
    isCollapsed?: boolean;
}

export const ROUTES: RouteInfo[] = [
    {
        path: '',
        title: 'Parâmetros',
        icon: 'fas fa-toolbox text-primary',
        class: '',
        childItems: [
            {
                path: '/produto',
                title: 'Produtos',
                icon: 'ni ni-briefcase-24 text-purple',
                class: ''
            },
            {
                path: '/tipo-acomodacao',
                title: 'Tipos de Acomodações',
                icon: 'ni ni-pin-3 text-orange',
                class: ''
            },
            {
                path: '/tipo-comprovante',
                title: 'Tipos de Comprovantes',
                icon: 'ni ni-money-coins text-success',
                class: ''
            },
            {
                path: '/tipo-evento',
                title: 'Tipos de Evento',
                icon: 'ni ni-planet text-blue',
                class: ''
            },
            {
                path: '/tipo-participante',
                title: 'Tipos de Participantes',
                icon: 'ni ni-single-02 text-yellow',
                class: ''
            }
        ],
        isCollapsed: true
    },
    {
        path: '/eventos',
        title: 'Eventos',
        icon: 'fas fa-calendar text-yellow',
        class: '',
        childItems: null,
        isCollapsed: false
    },
    {
        path: '/alocacoes/criar-de-pesquisa',
        title: 'Alocação',
        icon: 'ni-archive-2 text-green',
        class: '',
        isCollapsed: true,
        childItems: [
            {
                path: '/alocacoes/criar-de-pesquisa',
                title: 'Nova Alocação',
                icon: 'ni ni-single-02 text-yellow',
                class: ''
            },
            {
                path: '/alocacoes',
                title: 'Ver Alocações',
                icon: 'ni ni-single-02 text-yellow',
                class: ''
            }
        ],
    },
    {
        path: '/participantes',
        title: 'Participantes',
        icon: 'fas fa-user-friends text-red',
        class: '',
        childItems: null,
        isCollapsed: true
    }
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    public menuItems: any[];
    public isCollapsed = true;

    constructor(private router: Router) { }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        this.router.events.subscribe(() => {
            this.isCollapsed = true;
        });
    }
}
