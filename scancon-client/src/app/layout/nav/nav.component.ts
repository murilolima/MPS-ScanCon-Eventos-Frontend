import { Component } from '@angular/core';
import { TitleService } from 'src/app/core/services/title.service';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html'
})
export class NavComponent {

    public focus: any;

    constructor(private titleService: TitleService) { }

    getTitle() {
        return 'Eventos';
        // return this.titleService.getTitle();
    }
}
