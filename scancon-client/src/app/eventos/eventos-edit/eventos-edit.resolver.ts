import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { EventosService } from '../eventos.service';
import { Evento } from 'src/app/core/models/eventos/evento.model';
import { AlertsService } from 'src/app/core/services/alerts.service';

@Injectable()
export class EventosEditResolver implements Resolve<Evento> {
    constructor(
        private router: Router,
        private eventoService: EventosService,
        private alerts: AlertsService
    ) { }

    resolve(route: ActivatedRouteSnapshot): Observable<Evento> {
        return this.eventoService.getEvento(route.params.id).pipe(
            catchError(error => {
                this.alerts.warning('Não foi possível carregar evento: ' + route.params.id);
                this.router.navigate(['/eventos']);
                return of(null);
            })
        );
    }
}
