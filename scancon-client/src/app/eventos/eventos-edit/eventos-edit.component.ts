import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

import { EventosService } from '../eventos.service';
import { Evento } from 'src/app/core/models/eventos/evento.model';
import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoEvento } from 'src/app/core/models/parametros/tipo-evento.model';
import { TipoEventoService } from 'src/app/parametros/tipo-evento/tipo-evento.service';

@Component({
    selector: 'app-eventos-edit',
    templateUrl: './eventos-edit.component.html'
})
export class EventosEditComponent implements OnInit {
    evento: Evento = new Evento();
    loading = false;
    tipoEventos: TipoEvento[];

    editForm: FormGroup;

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.editForm.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private rxFormBuilder: RxFormBuilder,
        private eventoService: EventosService,
        private tipoEventoService: TipoEventoService,
        private route: ActivatedRoute,
        private router: Router,
        private alerts: AlertsService
    ) { }

    ngOnInit() {
        this.loading = true;
        this.route.data.subscribe(data => {
            this.loading = false;
            this.evento = data.evento;
        });
        this.editForm = this.rxFormBuilder.formGroup(this.evento);
        this.tipoEventoService.getAllTipoEventos().subscribe(tipoEventos => {
            this.tipoEventos = tipoEventos;
        });
    }

    onSave() {
        this.editForm.markAllAsTouched();
        if (this.editForm.invalid) {
            console.log('ta invalido amigo');
            return;
        }

        this.eventoService.updateEvento(this.evento).subscribe(
            next => {
                this.alerts.success('Evento alterado com sucesso!');
                this.editForm.markAsPristine();
                this.onBack();
            },
            error => {
                this.alerts.error(error);
            }
        );
    }

    onBack() {
        this.router.navigate(['/eventos']);
    }
}
