import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { Router } from '@angular/router';

import { Evento } from 'src/app/core/models/eventos/evento.model';
import { EventosService } from '../eventos.service';
import { AlertsService } from 'src/app/core/services/alerts.service';
import { TipoEvento } from 'src/app/core/models/parametros/tipo-evento.model';

// TODO: ver para mudar dependencia, (cross feature module import)
import { TipoEventoService } from 'src/app/parametros/tipo-evento/tipo-evento.service';

@Component({
    selector: 'app-eventos-create',
    templateUrl: './eventos-create.component.html'
})
export class EventosCreateComponent implements OnInit {
    registroEvento: FormGroup;
    evento = new Evento();
    tipoEventos: TipoEvento[];

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.registroEvento.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private eventoService: EventosService,
        private tipoEventoService: TipoEventoService,
        private router: Router,
        private alert: AlertsService,
        private rxFB: RxFormBuilder
    ) { }

    ngOnInit() {
        this.tipoEventoService.getAllTipoEventos().subscribe(tipoEventos => {
            this.tipoEventos = tipoEventos;
        });
        this.registroEvento = this.rxFB.formGroup(this.evento);
    }

    cadastraEvento() {
        if (this.registroEvento.invalid) {
            return;
        }

        if (this.registroEvento.valid) {
            this.evento = Object.assign({}, this.registroEvento.value);
            this.eventoService.createEvento(this.evento).subscribe(
                () => {
                    this.alert.success('Evento cadastrado com sucesso');
                    this.router.navigate(['/eventos']);
                },
                error => {
                    this.alert.error(error);
                }
            );
        }
    }

    voltar() {
        this.router.navigate(['/eventos']);
    }
}
