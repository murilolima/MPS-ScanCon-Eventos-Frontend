import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { NgxLoadingModule } from 'ngx-loading';
import { BsDatepickerModule, TooltipModule, ModalModule, BsLocaleService } from 'ngx-bootstrap';

import { FeatureModule } from '../core/feature-module.module';
import { SharedModule } from '../shared/shared.module';
import { EventosRoutingModule } from './eventos-routing.module';
import { EventosListComponent } from './eventos-list/eventos-list.component';
import { EventosCreateComponent } from './eventos-create/eventos-create.component';
import { EventosEditComponent } from './eventos-edit/eventos-edit.component';
import { EventosEditResolver } from './eventos-edit/eventos-edit.resolver';
import { EventosService } from './eventos.service';
import { NgxSettingsProvider, NgxSettings } from '../core/providers/ngx-settings.provider';

@NgModule({
    declarations: [EventosListComponent, EventosCreateComponent, EventosEditComponent],
    imports: [
        CommonModule,
        EventosRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        NgxLoadingModule,
        BsDatepickerModule.forRoot(),
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        SharedModule
    ],
    providers: [
        EventosService,
        EventosEditResolver,
        // TODO: ver como aplicar no módulo core
        NgxSettingsProvider,
        BsLocaleService,
        NgxSettings
    ]
})
export class EventosModule extends FeatureModule { }
