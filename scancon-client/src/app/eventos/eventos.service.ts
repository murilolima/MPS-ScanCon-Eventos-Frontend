import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { concatMap } from 'rxjs/operators';
import { of } from 'rxjs';

import { JsonDeserializer, JsonDeserializerFactory } from '../core/services/json-deserializer.service';
import { SettingsService } from '../core/services/settings.service';
import { Evento } from '../core/models/eventos/evento.model';
import { Saida } from '../core/models/eventos/saida.model';
import { Grupo } from '../core/models/eventos/grupo.model';

@Injectable({
    providedIn: 'root'
})
export class EventosService {
    baseUrl: string;
    deserializer: JsonDeserializer;

    constructor(
        private http: HttpClient,
        private settings: SettingsService,
        private jsonFactory: JsonDeserializerFactory) {
        this.baseUrl = this.settings.settings.apiUrl;
        this.deserializer = this.jsonFactory.construct(Evento);
    }

    getAllEventos() {
        return this.http.get<Evento[]>(this.baseUrl + 'eventos').pipe(
            concatMap(eventos => {
                return of(this.deserializer.deserialize(eventos));
            })
        );
    }

    getEvento(id: number) {
        return this.http.get<Evento>(this.baseUrl + `eventos/${id}`).pipe(
            concatMap(evento => {
                return of(this.deserializer.deserialize(evento));
            })
        );
    }

    createEvento(evento: Evento) {
        return this.http.post(this.baseUrl + 'eventos', evento, {
            responseType: 'text'
        });
    }

    updateEvento(evento: Evento) {
        return this.http.put(
            this.baseUrl + 'eventos',
            evento
        );
    }

    deleteEvento(id: number) {
        return this.http.delete(this.baseUrl + `eventos/${id}`, {responseType: 'text'});
    }

    getSaida(id: number) {
        return this.http.get<Saida>(this.baseUrl + `saidas/${id}`).pipe(
            concatMap(saida => {
                return of(this.deserializer.deserialize(saida));
            })
        );
    }

    getSaidas(eventoId: number) {
        return this.http.get<Saida[]>(this.baseUrl + `eventos/${eventoId}/saidas`).pipe(
            concatMap(saidas => {
                return of(this.deserializer.deserialize(saidas));
            })
        );
    }

    getGrupo(id: number) {
        return this.http.get<Grupo>(this.baseUrl + `grupos/${id}`).pipe(
            concatMap(grupo => {
                return of(this.deserializer.deserialize(grupo));
            })
        );
    }

    getGrupos(saidaId: number) {
        return this.http.get<Grupo[]>(this.baseUrl + `saidas/${saidaId}/grupos`).pipe(
            concatMap(grupos => {
                return of(this.deserializer.deserialize(grupos));
            })
        );
    }

    getAllGrupos(eventoId: number) {
        return this.http.get<Grupo[]>(this.baseUrl + `eventos/${eventoId}/grupos`).pipe(
            concatMap(grupos => {
                return of(this.deserializer.deserialize(grupos));
            })
        );
    }
}
