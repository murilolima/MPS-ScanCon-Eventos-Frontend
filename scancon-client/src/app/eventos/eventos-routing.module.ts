import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreventUnsavedChanges } from 'src/app/core/guards/prevent-unsaved-changed.guard';

import { EventosListComponent } from './eventos-list/eventos-list.component';
import { EventosCreateComponent } from './eventos-create/eventos-create.component';
import { EventosEditComponent } from './eventos-edit/eventos-edit.component';
import { EventosEditResolver } from './eventos-edit/eventos-edit.resolver';

const routes: Routes = [
    {
        path: '',
        component: EventosListComponent,
        runGuardsAndResolvers: 'always',
        data: { title: 'Eventos' }
    },
    {
        path: 'criar',
        component: EventosCreateComponent,
        data: { title: 'Novo Evento' }
    },
    {
        path: ':id/editar',
        component: EventosEditComponent,
        resolve: { evento: EventosEditResolver },
        canDeactivate: ['EventosEditComponent'],
        data: { title: 'Alterar Evento' }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [
        { provide: 'EventosEditComponent', useFactory: () => new PreventUnsavedChanges<EventosEditComponent>() }
    ]
})
export class EventosRoutingModule { }
