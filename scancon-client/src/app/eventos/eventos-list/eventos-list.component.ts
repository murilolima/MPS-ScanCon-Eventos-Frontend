import { Component, OnInit, Input } from '@angular/core';

import { EventosService } from '../eventos.service';
import { Evento } from 'src/app/core/models/eventos/evento.model';
import { AlertsService } from 'src/app/core/services/alerts.service';

@Component({
    selector: 'app-eventos-list',
    templateUrl: './eventos-list.component.html'
})
export class EventosListComponent implements OnInit {
    @Input() eve: Evento;
    eventos: any;
    loading = false;

    constructor(private eventoService: EventosService, private alerts: AlertsService) {
        this.eventos = [];
    }

    ngOnInit() {
        this.loadEventos();
    }

    loadEventos() {
        this.loading = true;
        this.eventoService.getAllEventos().subscribe(
            eventos => {
                this.loading = false;
                this.eventos = eventos;
            },
            error => {
                this.alerts.error(error);
            }
        );
    }

    deleteEventos(eventoId: number) {
        this.loading = true;
        this.eventoService.deleteEvento(eventoId).subscribe(
            () => {
                this.loading = false;
                this.eventos.splice(this.eventos.findIndex(p => p.id === eventoId), 1);
                this.alerts.success('Evento excluído com sucesso');
            },
            error => {
                this.loading = false;
                this.alerts.error(error);
            }
        );
    }
}
