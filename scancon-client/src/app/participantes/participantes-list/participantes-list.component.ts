import { Component, OnInit, Input } from '@angular/core';
import { ParticipantesService } from '../participantes.service';
import { AlertsService } from 'src/app/core/services/alerts.service';
import { Participante } from 'src/app/core/models/participante/participante.model';

@Component({
    selector: 'app-participantes-list',
    templateUrl: './participantes-list.component.html'
})
export class ParticipantesListComponent implements OnInit {
    @Input() parti: Participante;
    participantes: Participante[];

    loading = false;

    constructor(
        private participanteService: ParticipantesService,
        private alerts: AlertsService
    ) {}

    ngOnInit() {
        this.loadParticipantes();
    }

    loadParticipantes() {
        this.loading = true;
        this.participanteService.getAllParticipantes().subscribe(
            participantes => {
                this.loading = false;
                this.participantes = participantes;
            },
            error => {
                this.loading = false;
                this.alerts.error(error);
            }
        );
    }

    deleteParticipante(participanteId: string) {
        this.loading = true;
        this.participanteService.deleteParticipante(participanteId).subscribe(
            () => {
                this.loading = false;
                this.participantes.splice(
                    this.participantes.findIndex(p => p.id === participanteId),
                    1
                );
                this.alerts.success('Participante excluído com sucesso');
            },
            error => {
                this.loading = false;
                this.alerts.error(error);
            }
        );
    }
}
