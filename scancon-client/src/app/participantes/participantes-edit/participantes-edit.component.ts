import { Component, OnInit, HostListener, TemplateRef, ViewChild } from '@angular/core';
import { Participante } from 'src/app/core/models/participante/participante.model';
import { FormGroup, NgForm } from '@angular/forms';
import { RxFormBuilder, FormGroupExtension } from '@rxweb/reactive-form-validators';
import { ParticipantesService } from '../participantes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertsService } from 'src/app/core/services/alerts.service';
import { EnderecoParticipante } from 'src/app/core/models/participante/endereco-participante.model';
import { defineLocale, ptBrLocale } from 'ngx-bootstrap/chronos';
import { TipoParticipante } from 'src/app/core/models/parametros/tipo-participante.model';
import { TipoParticipanteService } from 'src/app/parametros/tipo-participante/tipo-participante.service';
defineLocale('pt-br', ptBrLocale);

@Component({
    selector: 'app-participantes-edit',
    templateUrl: './participantes-edit.component.html'
})
export class ParticipantesEditComponent implements OnInit {
    participante = new Participante();
    editForm: FormGroup;
    tipoParticipantes: TipoParticipante[];

    loading = false;

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.editForm.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private rxFB: RxFormBuilder,
        private participanteService: ParticipantesService,
        private tipoParticipanteService: TipoParticipanteService,
        private route: ActivatedRoute,
        private router: Router,
        private alerts: AlertsService
    ) {}

    ngOnInit() {
        this.loading = true;
        this.route.data.subscribe(data => {
            this.loading = false;
            this.participante = data.participante;
        });

        // Busca todos os tipos de participante
        this.tipoParticipanteService.getAllTipoParticipantes().subscribe(tipoParticipantes => {
            this.tipoParticipantes = tipoParticipantes;
        });

        if (this.participante.sexo === '109' || this.participante.sexo === 'Masculino') {
            this.participante.semanasGravidez = '0';
            this.participante.gestante = 'false';
        }

        this.participante.tipoParticipanteId = this.participante.tipoParticipanteId;
        this.editForm = this.rxFB.formGroup(this.participante);
        this.resetForm();
    }

    reset() {
        (<FormGroupExtension>this.editForm).resetForm();
    }

    onSave() {
        if (this.editForm.invalid) {
            this.editForm.markAllAsTouched();
            return;
        }

        this.participanteService.updateParticipante(this.participante).subscribe(
            () => {
                this.alerts.success('Participante atualizado com sucesso');
                this.reset();
                this.editForm.markAsUntouched();
                this.editForm.markAsPristine();
                this.onBack();
            },
            error => {
                this.alerts.error(error);
            }
        );
    }

    onBack() {
        this.router.navigate(['/participantes']);
    }

    resetForm() {
        this.habilitaCamposGenero(this.participante.sexo === 'Masculino' ? 1 : 0);
        this.habilitaCamposPassaporte(this.participante.passaporte !== '' ? this.participante.passaporte : '');
        this.habilitaCamposVisto(this.participante.visto.toString() === 'true' ? 'true' : 'false');
    }

    habilitaCamposGenero(x: number) {
        if (x === 1) {
            // this.disabledPorSexo = true;
            this.editForm.controls.gestante.disable();
            this.editForm.controls.semanasGravidez.disable();
        }
        if (x === 0) {
            this.editForm.controls.gestante.enable();
            if (this.editForm.value.gestante === 'false') {
                this.editForm.controls.semanasGravidez.disable();
            }
            if (this.editForm.value.gestante === 'true') {
                this.editForm.controls.semanasGravidez.enable();
            }
        }
    }

    habilitaCamposGravidez(x: string) {
        if (x === 'false') {
            this.editForm.controls.semanasGravidez.disable();
        }
        if (x === 'true') {
            this.editForm.controls.semanasGravidez.enable();
        }
    }

    habilitaCamposPassaporte(x: string) {
        if (x === '') {
            this.editForm.controls.passaporteValidade.disable();
            this.editForm.controls.passaporteValidade.setValue('');
        } else {
            this.editForm.controls.passaporteValidade.enable();
        }
    }

    habilitaCamposVisto(x: string) {
        if (x === 'false') {
            this.editForm.controls.vistoValidade.disable();
        }
        if (x === 'true') {
            this.editForm.controls.vistoValidade.enable();
        }
    }
}
