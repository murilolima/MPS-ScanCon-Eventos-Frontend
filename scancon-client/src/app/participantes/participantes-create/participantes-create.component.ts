import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TipoParticipante } from 'src/app/core/models/parametros/tipo-participante.model';
import { ParticipantesService } from '../participantes.service';
import { TipoParticipanteService } from 'src/app/parametros/tipo-participante/tipo-participante.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertsService } from 'src/app/core/services/alerts.service';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { Participante } from 'src/app/core/models/participante/participante.model';
import { EnderecoParticipante } from 'src/app/core/models/participante/endereco-participante.model';
import { defineLocale, ptBrLocale } from 'ngx-bootstrap/chronos';
defineLocale('pt-br', ptBrLocale);

@Component({
    selector: 'app-participantes-create',
    templateUrl: './participantes-create.component.html'
})
export class ParticipantesCreateComponent implements OnInit {
    // Propriedades dos Forms
    registroParticipante: FormGroup;
    participante = new Participante();
    tipoParticipantes: TipoParticipante[];
    backUrl: string;

    @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
        if (this.registroParticipante.dirty) {
            $event.returnValue = true;
        }
    }

    constructor(
        private participanteService: ParticipantesService,
        private tipoParticipanteService: TipoParticipanteService,
        private route: ActivatedRoute,
        private router: Router,
        private alerts: AlertsService,
        private rxFB: RxFormBuilder
    ) { }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.backUrl = unescape(params.backUrl);
        });

        this.tipoParticipanteService.getAllTipoParticipantes().subscribe(tipoParticipante => {
            this.tipoParticipantes = tipoParticipante;
        });
        this.participante.visto = '';
        this.participante.gestante = '';
        this.participante.semanasGravidez = '0';
        this.participante.necessidadesEspeciais = undefined;
        this.participante.tipoParticipanteId = '';
        this.participante.endereco = new EnderecoParticipante();
        this.registroParticipante = this.rxFB.formGroup(this.participante);
        this.registroParticipante.controls.passaporteValidade.disable();
        this.registroParticipante.controls.gestante.disable();
        this.registroParticipante.controls.semanasGravidez.disable();
        this.registroParticipante.controls.vistoValidade.disable();
    }

    cadastraParticipante() {
        this.registroParticipante.markAllAsTouched();

        if (this.registroParticipante.invalid) {
            return;
        }

        if (this.registroParticipante.valid) {
            this.participante = Object.assign({}, this.registroParticipante.value);
            console.log(this.participante);

            this.participanteService.createParticipante(this.participante).subscribe(
                () => {
                    this.alerts.success('Participante cadastrado com sucesso');
                    setTimeout(() => {
                        this.voltar();
                    }, 1000);
                },
                error => {
                    this.alerts.error(error);
                }
            );
        }
    }

    voltar() {
        if (this.backUrl) {
            return this.router.navigateByUrl(this.backUrl);
        }
        this.router.navigate(['/participantes']);
    }

    habilitaCamposGenero(x: number) {
        if (x === 1) {
            // this.disabledPorSexo = true;
            this.registroParticipante.controls.gestante.disable();
            this.registroParticipante.controls.semanasGravidez.disable();
        }
        if (x === 0) {
            this.registroParticipante.controls.gestante.enable();
            if (this.registroParticipante.value.gestante === 'false') {
                this.registroParticipante.controls.semanasGravidez.disable();
            }
            if (this.registroParticipante.value.gestante === 'true') {
                this.registroParticipante.controls.semanasGravidez.enable();
            }
        }
    }

    habilitaCamposGravidez(x: string) {
        if (x === 'false') {
            this.registroParticipante.controls.semanasGravidez.disable();
        }
        if (x === 'true') {
            this.registroParticipante.controls.semanasGravidez.enable();
        }
    }

    habilitaCamposPassaporte(x: string) {
        if (x === '') {
            this.registroParticipante.controls.passaporteValidade.disable();
            this.registroParticipante.controls.passaporteValidade.setValue('');
        } else {
            this.registroParticipante.controls.passaporteValidade.enable();
        }
    }

    habilitaCamposVisto(x: string) {
        if (x === 'false') {
            this.registroParticipante.controls.vistoValidade.disable();
        }
        if (x === 'true') {
            this.registroParticipante.controls.vistoValidade.enable();
        }
    }
}
