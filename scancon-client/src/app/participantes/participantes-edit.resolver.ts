import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Participante } from 'src/app/core/models/participante/participante.model';
import { ParticipantesService } from './participantes.service';
import { AlertsService } from 'src/app/core/services/alerts.service';

@Injectable()
export class ParticipantesEditResolver implements Resolve<Participante> {
    constructor(
        private router: Router,
        private participanteService: ParticipantesService,
        private alerts: AlertsService
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Participante> {
        return this.participanteService.getParticipante(route.params.id).pipe(
            catchError(error => {
                this.alerts.warning(error);
                this.router.navigate(['/participantes']);
                return of(null);
            })
        );
    }
}
