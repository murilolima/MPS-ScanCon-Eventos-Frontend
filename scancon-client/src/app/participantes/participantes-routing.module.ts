import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParticipantesListComponent } from './participantes-list/participantes-list.component';
import { ParticipantesCreateComponent } from './participantes-create/participantes-create.component';
import { ParticipantesEditComponent } from './participantes-edit/participantes-edit.component';
import { ParticipantesEditResolver } from './participantes-edit.resolver';
import { PreventUnsavedChanges } from '../core/guards/prevent-unsaved-changed.guard';

const routes: Routes = [
    {
        path: '',
        component: ParticipantesListComponent,
        runGuardsAndResolvers: 'always',
        data: { title: 'Participantes' }
    },
    {
        path: 'criar',
        component: ParticipantesCreateComponent,
        data: { title: 'Novo Participante' }
    },
    {
        path: ':id/editar',
        component: ParticipantesEditComponent,
        resolve: { participante: ParticipantesEditResolver },
        canDeactivate: ['ParticipantesEditComponent'],
        data: { title: 'Alterar Participante' }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [
        {
            provide: 'ParticipantesEditComponent',
            useFactory: () =>
                new PreventUnsavedChanges<ParticipantesEditComponent>()
        }
    ]
})
export class ParticipantesRoutingModule { }
