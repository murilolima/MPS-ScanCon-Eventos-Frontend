import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TooltipModule, ModalModule, BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { NgxMaskModule } from 'ngx-mask';
import { NgxLoadingModule } from 'ngx-loading';

import { FeatureModule } from '../core/feature-module.module';
import { ParticipantesCreateComponent } from './participantes-create/participantes-create.component';
import { ParticipantesListComponent } from './participantes-list/participantes-list.component';
import { ParticipantesEditComponent } from './participantes-edit/participantes-edit.component';
import { ParticipantesRoutingModule } from './participantes-routing.module';
import { PreventUnsavedChanges } from 'src/app/core/guards/prevent-unsaved-changed.guard';
import { ParticipantesService } from './participantes.service';
import { ParticipantesEditResolver } from './participantes-edit.resolver';
import { NgxSettingsProvider, NgxSettings } from '../core/providers/ngx-settings.provider';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [ParticipantesCreateComponent, ParticipantesListComponent, ParticipantesEditComponent],
    imports: [
        CommonModule,
        ParticipantesRoutingModule,
        FormsModule,
        SharedModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        NgxLoadingModule,
        BsDatepickerModule.forRoot(),
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        NgxMaskModule.forRoot(),
        SharedModule
    ],
    providers: [
        ParticipantesService,
        ParticipantesEditResolver,
        PreventUnsavedChanges,
        NgxSettingsProvider,
        BsLocaleService,
        NgxSettings
    ]
})
export class ParticipantesModule extends FeatureModule { }
