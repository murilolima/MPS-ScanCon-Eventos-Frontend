import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { concatMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { SettingsService } from 'src/app/core/services/settings.service';
import { Participante } from 'src/app/core/models/participante/participante.model';
import { JsonDeserializer, JsonDeserializerFactory } from 'src/app/core/services/json-deserializer.service';
import { ParticipanteSearch } from '../shared/participantes-search-dialog/participante.search.model';

@Injectable({
    providedIn: 'root'
})
export class ParticipantesService {
    // URL da API
    baseUrl: string;
    deserializer: JsonDeserializer;

    constructor(
        private http: HttpClient,
        private settings: SettingsService,
        private jsonFactory: JsonDeserializerFactory) {
        this.baseUrl = this.settings.settings.apiUrl;
        this.deserializer = this.jsonFactory.construct(Participante);
    }

    // Método para retornar todos os participantes
    getAllParticipantes() {
        return this.http.get<Participante[]>(this.baseUrl + 'participantes');
    }

    getParticipantes(searchModel: ParticipanteSearch) {
        return this.http.get<Participante[]>(this.baseUrl + 'participantes').pipe(
            concatMap(participantes => {
                if (searchModel.nomeCpfCnpj) {
                    participantes = participantes.filter(p => {
                        const isNome = new RegExp(searchModel.nomeCpfCnpj, 'i').test(p.nomeCompleto);
                        const isCpfCnpj = new RegExp(searchModel.nomeCpfCnpj, 'i').test(p.cpf);
                        return isNome || isCpfCnpj;
                    });
                }
                return of(participantes);
            })
        );
    }

    // Método para retornar um participante específico
    getParticipante(id: string): Observable<Participante> {
        return this.http
            .get<Participante>(this.baseUrl + 'participantes/' + id)
            .pipe(
                concatMap(participante => {
                    return of(this.deserializer.deserialize(participante));
                })
            );
    }

    // Método para criar participante
    createParticipante(participante: Participante) {
        return this.http.post(this.baseUrl + 'participantes', participante, {
            responseType: 'text'
        });
    }

    // Método para atualizar participante
    updateParticipante(participante: Participante) {
        return this.http.put(
            this.baseUrl + 'participantes/',
            participante, { responseType: 'text' }
        );
    }

    // Método para deletar um participante
    deleteParticipante(id: string) {
        return this.http.delete(this.baseUrl + 'participantes/' + id);
    }
}
