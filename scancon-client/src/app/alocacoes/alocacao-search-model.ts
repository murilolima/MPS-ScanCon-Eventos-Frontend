import { required, alphaNumeric, prop } from '@rxweb/reactive-form-validators';

export class AlocacaoSearch {

    @required()
    eventoId: number;

    @required()
    grupoId: number;

    @prop()
    saidaId: number;

    @alphaNumeric()
    grupo: string;

    @alphaNumeric()
    cota: string;

    @prop()
    cpfCnpj: string;
}
