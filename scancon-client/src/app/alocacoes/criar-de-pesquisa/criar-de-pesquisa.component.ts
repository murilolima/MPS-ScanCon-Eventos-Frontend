import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { Cota } from 'src/app/core/models/eventos/cota.model';
import { Evento } from 'src/app/core/models/eventos/evento.model';
import { CotasStore } from 'src/app/core/stores/cotas.store';
import { EventosStore } from 'src/app/core/stores/eventos.store';
import { CotasService } from '../cotas.service';
import { CotaSearch } from '../cota-search.model';

@Component({
    selector: 'app-criar-de-pesquisa',
    templateUrl: './criar-de-pesquisa.component.html'
})
export class CriarDePesquisaComponent implements OnInit {

    cotaSearch: CotaSearch;

    cotas: Cota[];
    selectedCotas: Cota[];

    eventos: Evento[];
    evento: Evento;

    loading = false;

    constructor(
        private cotasService: CotasService,
        private cotasStore: CotasStore,
        private eventosStore: EventosStore,
        private alerts: AlertsService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.loading = true;

        // TODO: criar botão para desselecionar
        // this.cotasStoreService.unselectAllCotas();

        this.cotasStore.selectedCotas$.subscribe(
            cotas => this.selectedCotas = cotas
        );

        this.eventosStore.selectedEvento$.subscribe(
            evento => this.evento = evento
        );

        this.route.data.subscribe(data => {
            this.loading = false;
            this.eventos = data.eventos;
        });

        this.cotaSearch = new CotaSearch();
        this.route.queryParams.subscribe(params => {
            this.mapCotaSearchFromParams(params);
            this.searchCotas(this.cotaSearch);
        });
        this.loading = false;
    }

    mapCotaSearchFromParams(params: Params) {
        this.cotaSearch.eventoId = params.eventoId;
        this.cotaSearch.grupo = params.grupo;
        this.cotaSearch.cota = params.cota;
        this.cotaSearch.cpfCnpj = params.cpfCnpj;
    }

    searchCotas(cotaSearch: CotaSearch) {
        this.loading = true;
        this.updateUrlQuery(cotaSearch);
        return this.cotasService
            .getCotas(cotaSearch)
            .subscribe(
                cotas => {
                    this.loading = false;
                    this.cotas = cotas;
                    this.initSelectedCotas();
                },
                error => {
                    this.loading = false;
                    this.alerts.error(error);
                }
            );
    }

    // TODO: Mover lógica para cotasStore, após buscar cotas
    initSelectedCotas() {
        this.cotas.map(cota => this.cotasStore.assignCotaSelection(cota));
    }

    selectEvento(evento: Evento) {
        this.eventosStore.selectEvento(evento);
    }

    updateUrlQuery(value: any) {
        this.router.navigate([], {
            queryParams: value,
            queryParamsHandling: 'merge'
        });
    }

    selectCota(cota: Cota) {
        this.cotasStore.selectCota(cota);
    }

    unSelectCota(cota: Cota) {
        this.cotasStore.unSelectCota(cota);
    }

    create() {
        const extras = { queryParams: this.cotaSearch };
        this.router.navigate(['alocacoes/criar'], extras);
    }
}
