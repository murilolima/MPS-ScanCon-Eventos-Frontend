import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { FormGroup } from '@angular/forms';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { CotasStore } from 'src/app/core/stores/cotas.store';
import { EventosStore } from 'src/app/core/stores/eventos.store';
import { AlocacaoCreateModel } from './alocacao-create.model';

import { AlocacoesService } from '../alocacoes.service';

@Component({
    selector: 'app-alocacoes-create',
    templateUrl: './alocacoes-create.component.html'
})
export class AlocacoesCreateComponent implements OnInit {

    loading: boolean;
    alocacaoCreate = new AlocacaoCreateModel();
    createForm: FormGroup;

    backParams: any;

    constructor(
        private cotasStore: CotasStore,
        private eventosStore: EventosStore,
        private alerts: AlertsService,
        private alocacoesService: AlocacoesService,
        private router: Router,
        private route: ActivatedRoute,
        private rxFormBuilder: RxFormBuilder
    ) { }

    ngOnInit() {
        this.loading = true;

        this.route.data.subscribe(data => {
            this.loading = false;
            this.alocacaoCreate.grupos = data.grupos;
        });

        this.route.queryParams.subscribe(params => {
            this.backParams = params;
        });

        this.cotasStore.selectedCotas$.subscribe(
            cotas => this.alocacaoCreate.cotas = cotas
        );

        this.alocacaoCreate.evento = this.eventosStore.selectedEvento;
        this.createForm = this.rxFormBuilder.formGroup(this.alocacaoCreate);
    }

    onCreate() {
        if (this.createForm.invalid) {
            return;
        }

        this.alocacaoCreate.setGrupo();
        this.alocacoesService.createAlocacao(this.alocacaoCreate).subscribe(
            (alocacaoId) => {
                this.cotasStore.unselectAllCotas();
                this.alerts.success('Alocação cadastrada com sucesso');
                const params = {
                    eventoId: this.alocacaoCreate.evento.id,
                    grupoId: this.alocacaoCreate.grupo.id,
                    saidaId: this.alocacaoCreate.saida.id
                };
                this.router.navigate([`alocacoes/${alocacaoId}`], { queryParams: params });
            },
            error => {
                this.alerts.error(error);
            }
        );
    }

    onBack() {
        this.router.navigate(['alocacoes/criar-de-pesquisa'], { queryParams: this.backParams });
    }
}
