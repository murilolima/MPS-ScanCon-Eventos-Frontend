import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { EventosService } from 'src/app/eventos/eventos.service';
import { EventosStore } from 'src/app/core/stores/eventos.store';
import { Grupo } from 'src/app/core/models/eventos/grupo.model';


export class AlocacoesCreateResolver implements Resolve<Grupo[]> {

    constructor(
        private router: Router,
        private eventosService: EventosService,
        private eventosStore: EventosStore,
        private alerts: AlertsService
    ) { }

    resolve(route: ActivatedRouteSnapshot): Observable<Grupo[]> {
        const evento = this.eventosStore.selectedEvento;
        if (evento == null) {
            this.alerts.warning('Não foi possível carregar informações do evento');
            this.router.navigate(['alocacoes/criar-de-pesquisa'], { queryParams: route.queryParams });
            return of(null);
        }

        return this.eventosService.getAllGrupos(evento.id).pipe(
            catchError(error => {
                console.log(error);
                this.alerts.warning('Não foi possível carregar os grupos do evento');
                this.router.navigate(['alocacoes/criar-de-pesquisa'], { queryParams: route.queryParams });
                return of(null);
            })
        );
    }

}
