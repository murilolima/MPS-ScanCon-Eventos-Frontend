import { required } from '@rxweb/reactive-form-validators';

import { Cota } from 'src/app/core/models/eventos/cota.model';
import { Grupo } from 'src/app/core/models/eventos/grupo.model';
import { Evento } from 'src/app/core/models/eventos/evento.model';
import { Participante } from 'src/app/core/models/participante/participante.model';
import { Alocacao } from 'src/app/core/models/eventos/alocacao.model';

export class AlocacaoCreateModel implements Alocacao {

    id: number;
    numAlocacao: number;
    grupo: Grupo;
    cotas: Cota[];

    // specific create attribute
    @required()
    grupoId: number;

    // specific create members
    grupos: Grupo[];
    evento: Evento;

    participantes: Participante[];

    get saida() {
        return this.grupo.saida;
    }

    setGrupo() {
        // TODO: deixar com 2 iguais ==
        this.grupo = this.grupos.filter(g => g.id == this.grupoId as number).pop();
    }

    addParticipante(participante: Participante): void {
        throw new Error('Method not implemented.');
    }

    containsParticipante(participante: Participante): Participante {
        throw new Error('Method not implemented.');
    }
}
