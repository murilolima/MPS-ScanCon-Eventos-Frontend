import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';

import { SharedModule } from '../shared/shared.module';
import { CotasRoutingModule } from './alocacoes-routing.module';
import { CriarDePesquisaComponent } from './criar-de-pesquisa/criar-de-pesquisa.component';
import { EventosModule } from '../eventos/eventos.module';
import { CriarDePesquisaResolver } from './criar-de-pesquisa/criar-de-pesquisa.resolver';
import { CotasSearchComponent } from './cotas-search/cotas-search.component';
import { CotasListComponent } from './cotas-list/cotas-list.component';
import { AlocacoesCreateComponent } from './alocacoes-create/alocacoes-create.component';
import { AlocacoesCreateResolver } from './alocacoes-create/alocacoes-create.resolver';
import { AlocacoesDetailsComponent } from './alocacoes-details/alocacoes-details.component';
import { AlocacoesDetailsResolver } from './alocacoes-details/alocacoes-details.resolver';
import { AlocacoesListComponent } from './alocacoes-list/alocacoes-list.component';
import { AlocacoesSearchComponent } from './alocacoes-search/alocacoes-search.component';
import { AlocacoesListResolver } from './alocacoes-list/alocacoes-list.resolver';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
    declarations: [
        CriarDePesquisaComponent,
        CotasSearchComponent,
        CotasListComponent,
        AlocacoesCreateComponent,
        AlocacoesDetailsComponent,
        AlocacoesListComponent,
        AlocacoesSearchComponent
    ],
    imports: [
        CommonModule,
        CotasRoutingModule,
        ReactiveFormsModule,
        NgxLoadingModule,
        RxReactiveFormsModule,
        SharedModule,
        EventosModule,
        RouterModule
    ],
    providers: [
        CriarDePesquisaResolver,
        AlocacoesCreateResolver,
        AlocacoesDetailsResolver,
        AlocacoesListResolver
    ]
})
export class AlocacoesModule { }
