import { required, alphaNumeric, prop } from '@rxweb/reactive-form-validators';

export class CotaSearch {

    @required()
    eventoId: number;

    @alphaNumeric()
    grupo: string;

    @alphaNumeric()
    cota: string;

    @prop()
    cpfCnpj: string;
}
