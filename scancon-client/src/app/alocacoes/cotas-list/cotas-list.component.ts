import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Cota } from 'src/app/core/models/eventos/cota.model';

@Component({
    selector: 'app-cotas-list',
    templateUrl: './cotas-list.component.html'
})
export class CotasListComponent {

    @Input()
    cotas: Cota[];

    @Input()
    selectable: boolean;

    @Output("selectedCota")
    selectCotaEmitter = new EventEmitter();

    @Output("unSelectedCota")
    unSlectCotaEmitterCotaEmitter = new EventEmitter();

    constructor() { }

    selectCota(cota: Cota) {
        if (!cota.selected) {
            this.selectCotaEmitter.emit(cota);
        } else {
            this.unSlectCotaEmitterCotaEmitter.emit(cota);
        }
    }
}
