import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { EventosService } from 'src/app/eventos/eventos.service';
import { Alocacao } from 'src/app/core/models/eventos/alocacao.model';
import { AlocacoesService } from '../alocacoes.service';
import { Participante } from 'src/app/core/models/participante/participante.model';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-alocacoes-details',
    templateUrl: './alocacoes-details.component.html'
})
export class AlocacoesDetailsComponent implements OnInit {
    alocacao: Alocacao;

    openParticipantesTrigger: Subject<void> = new Subject<void>();
    closeParticipantesTrigger: Subject<void> = new Subject<void>();

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private eventosService: EventosService,
        private alocacoesService: AlocacoesService,
    ) { }

    ngOnInit() {
        this.route.data.subscribe(data => {
            this.alocacao = data.alocacao;
        });

        // TODO: ver como otimizar estas consultas
        this.route.queryParams.subscribe(params => {
            this.eventosService.getGrupo(params.grupoId).subscribe(grupo => {
                this.alocacao.grupo = grupo;
                this.eventosService.getSaida(params.saidaId).subscribe(saida => {
                    this.alocacao.grupo.saida = saida;
                    this.eventosService.getEvento(params.eventoId).subscribe(evento => {
                        this.alocacao.grupo.saida.evento = evento;
                    });
                });
            });
        });
    }

    onSelectParticipante(participante: Participante) {
        this.alocacoesService.addParticipante(this.alocacao, participante).subscribe(() => {
            this.closeParticipantes();
        });
    }

    openParticipantes() {
        this.openParticipantesTrigger.next();
    }

    closeParticipantes() {
        this.closeParticipantesTrigger.next();
    }

    onRemoveParticipante(participante: Participante) {
        this.alocacoesService.removeParticipante(this.alocacao, participante).subscribe(() => {
            const participantes = this.alocacao.participantes.filter(p => p.id != participante.id);
            this.alocacao.participantes = participantes;
        });
    }

    newParticipante() {
        this.closeParticipantesTrigger.next();
        const extras = { backUrl: this.router.url };
        this.router.navigate(['participantes/criar'], { queryParams: extras });
    }
}
