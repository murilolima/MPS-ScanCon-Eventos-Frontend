import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AlertsService } from 'src/app/core/services/alerts.service';
import { AlocacoesService } from '../alocacoes.service';
import { Alocacao } from 'src/app/core/models/eventos/alocacao.model';

@Injectable()
export class AlocacoesDetailsResolver implements Resolve<Alocacao> {

    constructor(
        private router: Router,
        private alocacoesService: AlocacoesService,
        private alerts: AlertsService
    ) { }

    resolve(route: ActivatedRouteSnapshot): Observable<Alocacao> {
        const id = route.params.id;
        return this.alocacoesService.getAlocacao(id).pipe(
            catchError(error => {
                console.log(error);
                this.alerts.warning('Não foi possível carregar a alocação: ' + route.params.id);
                this.router.navigate(['/alocacoes']);
                return of(null);
            })
        );
    }
}
