import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CriarDePesquisaComponent } from './criar-de-pesquisa/criar-de-pesquisa.component';
import { CriarDePesquisaResolver } from './criar-de-pesquisa/criar-de-pesquisa.resolver';
import { AlocacoesCreateComponent } from './alocacoes-create/alocacoes-create.component';
import { AlocacoesCreateResolver } from './alocacoes-create/alocacoes-create.resolver';
import { AlocacoesDetailsComponent } from './alocacoes-details/alocacoes-details.component';
import { AlocacoesDetailsResolver } from './alocacoes-details/alocacoes-details.resolver';
import { AlocacoesListComponent } from './alocacoes-list/alocacoes-list.component';
import { AlocacoesListResolver } from './alocacoes-list/alocacoes-list.resolver';

const routes: Routes = [
    {
        path: '',
        component: AlocacoesListComponent,
        resolve: { eventos: AlocacoesListResolver },
        runGuardsAndResolvers: 'always',
        data: { title: 'Alocações' }
    },
    {
        path: 'criar-de-pesquisa',
        component: CriarDePesquisaComponent,
        resolve: { eventos: CriarDePesquisaResolver },
        runGuardsAndResolvers: 'always',
        data: { title: 'Nova Alocação' }
    },
    {
        path: 'criar',
        component: AlocacoesCreateComponent,
        resolve: { grupos: AlocacoesCreateResolver },
        runGuardsAndResolvers: 'always',
        data: { title: 'Nova Alocação' }
    },
    {
        path: ':id',
        component: AlocacoesDetailsComponent,
        resolve: { alocacao: AlocacoesDetailsResolver },
        runGuardsAndResolvers: 'always',
        data: { title: 'Alocação' }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CotasRoutingModule { }
