import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

import { SettingsService } from 'src/app/core/services/settings.service';
import { Cota } from 'src/app/core/models/eventos/cota.model';
import { CotaSearch } from './cota-search.model';

@Injectable({
    providedIn: 'root'
})
export class CotasService {
    baseUrl: string;

    constructor(private http: HttpClient, private settings: SettingsService) {
        this.baseUrl = this.settings.settings.apiUrl;
    }

    getCotas(searchModel: CotaSearch) {
        if (!searchModel.eventoId) {
            return of([]);
        }

        const queryParams = {
            codCota: searchModel.cota || '',
            grupoCota: searchModel.grupo || '',
            cpfCnpj: searchModel.cpfCnpj || ''
        };

        return this.http.get<Cota[]>(this.baseUrl + 'cotas', { params: queryParams });
    }
}
