import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { concatMap } from 'rxjs/operators';
import { of } from 'rxjs';

import { AlocacaoSearch } from './alocacao-search-model';
import { SettingsService } from 'src/app/core/services/settings.service';
import { Alocacao } from 'src/app/core/models/eventos/alocacao.model';
import { Participante } from 'src/app/core/models/participante/participante.model';
import { TipoParticipante } from 'src/app/core/models/parametros/tipo-participante.model';
import { JsonDeserializer, JsonDeserializerFactory } from '../core/services/json-deserializer.service';

@Injectable({
    providedIn: 'root'
})
export class AlocacoesService {
    baseUrl: string;
    deserializer: JsonDeserializer;

    constructor(
        private http: HttpClient,
        private settings: SettingsService,
        private jsonFactory: JsonDeserializerFactory) {
        this.baseUrl = this.settings.settings.apiUrl;
        this.deserializer = this.jsonFactory.construct(Alocacao);
    }

    // TODO: Adicionar validações e mensagens de erro apropriadas
    // TODO: Ajustar saida, atualmente alocacao.grupo.saida.eventoId está undefined
    createAlocacao(alocacao: Alocacao) {
        const body = { grupoId: alocacao.grupoId, cotas: alocacao.cotas };
        return this.http.post<number>(this.baseUrl + 'alocacoes', body).pipe(
            concatMap(alocacaoId => {
                alocacao.id = alocacaoId;
                for (const cota of alocacao.cotas) {
                    cota.alocacaoGrupocotaId = alocacaoId;
                }
                return of(alocacaoId);
            })
        );
    }

    getAlocacao(id: number) {
        return this.http.get<Alocacao[]>(this.baseUrl + `alocacoes/${id}`).pipe(
            concatMap(alocacao => {
                return of(this.deserializer.deserialize(alocacao));
            })
        );
    }

    // TODO: Implementar filtros de campos da cota
    getAlocacoes(searchModel: AlocacaoSearch) {
        const eventoId = searchModel.eventoId;
        const saidaId = searchModel.saidaId;
        const grupoId = searchModel.grupoId;

        if (!eventoId || !saidaId || !grupoId) {
            return of([]);
        }

        const url = `grupos/${grupoId}/alocacoes`;

        return this.http.get<Alocacao[]>(this.baseUrl + url).pipe(
            concatMap(alocacoes => {
                if (searchModel.cota) {
                    alocacoes = alocacoes.filter(a => a.cotas.some(cota =>
                        new RegExp(searchModel.cota, 'i').test(cota.codCota))
                    );
                }

                if (searchModel.grupo) {
                    alocacoes = alocacoes.filter(a => a.cotas.some(cota =>
                        new RegExp(searchModel.grupo, 'i').test(cota.grupo))
                    );
                }

                if (searchModel.cpfCnpj) {
                    alocacoes = alocacoes.filter(a => a.cotas.some(cota =>
                        new RegExp(searchModel.cpfCnpj, 'i').test(cota.consorciado))
                    );
                }

                return of(alocacoes);
            })
        );
    }

    async getParticipantes(alocacao: Alocacao) {
        const eventoId = alocacao.grupo.saida.evento.id;
        const saidaId = alocacao.grupo.saida.id;
        const grupoId = alocacao.grupo.id;
        const id = alocacao.id;

        if (!eventoId || !saidaId || !grupoId) {
            return [];
        }

        const url = `alocacoes/${id}/participantes`;

        const participantes = await this.http.get<Participante[]>(this.baseUrl + url).toPromise();
        for (const participante of participantes) {
            const urlParticipante = `parametros/tipoParticipante/${participante.tipoParticipanteId}`;
            const tipo = await this.http.get<TipoParticipante>(this.baseUrl + urlParticipante).toPromise();
            participante.tipoParticipanteDesc = tipo.descTipoParticipante;
            participante.tipoParticipanteId = tipo.id;
        }

        return participantes;
    }

    addParticipante(alocacao: Alocacao, participante: Participante) {
        if (alocacao.containsParticipante(participante)) {
            return of(null);
        }
        alocacao.addParticipante(participante);

        const alocacaoId = alocacao.id;
        const participanteId = participante.id;

        const url = `alocacoes/${alocacaoId}/participantes/${participanteId}`;

        return this.http.post(this.baseUrl + url, {});
    }

    removeParticipante(alocacao: Alocacao, participante: Participante) {
        const alocacaoId = alocacao.id;
        const participanteId = participante.id;

        const url = `alocacoes/${alocacaoId}/participantes/${participanteId}`;

        return this.http.delete(this.baseUrl + url, {});
    }
}
