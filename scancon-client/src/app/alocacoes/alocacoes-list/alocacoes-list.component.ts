import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { AlertsService } from 'src/app/core/services/alerts.service';

import { Evento } from 'src/app/core/models/eventos/evento.model';
import { AlocacaoSearch } from '../alocacao-search-model';
import { Alocacao } from 'src/app/core/models/eventos/alocacao.model';
import { AlocacoesService } from '../alocacoes.service';
import { Grupo } from 'src/app/core/models/eventos/grupo.model';
import { EventosService } from 'src/app/eventos/eventos.service';
import { EventosStore } from 'src/app/core/stores/eventos.store';

@Component({
    selector: 'app-alocacoes-list',
    templateUrl: './alocacoes-list.component.html'
})
export class AlocacoesListComponent implements OnInit {

    searchModel: AlocacaoSearch;

    alocacao: Alocacao;
    alocacoes: Alocacao[];

    eventos: Evento[];
    evento: Evento;

    grupos: Grupo[];
    grupo: Grupo;

    loading: boolean;

    constructor(
        private alocacoesService: AlocacoesService,
        private eventosService: EventosService,
        private eventosStore: EventosStore,
        private alerts: AlertsService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.loading = true;

        this.route.data.subscribe(data => {
            this.loading = false;
            this.eventos = data.eventos;
        });

        this.searchModel = new AlocacaoSearch();
        this.route.queryParams.subscribe(params => {
            this.mapCotaSearchFromParams(params);
            this.searchAlocacoes(this.searchModel);
        });
    }

    mapCotaSearchFromParams(params: Params) {
        this.searchModel.eventoId = params.eventoId;
        this.searchModel.grupoId = params.grupoId;
        this.searchModel.saidaId = params.saidaId;
        this.searchModel.grupo = params.grupo;
        this.searchModel.cota = params.cota;
        this.searchModel.cpfCnpj = params.cpfCnpj;
    }

    searchAlocacoes(searchModel: AlocacaoSearch) {
        this.loading = true;
        this.updateUrlQuery(searchModel);
        return this.alocacoesService
            .getAlocacoes(searchModel)
            .subscribe(
                alocacoes => {
                    this.loading = false;
                    this.alocacoes = alocacoes;
                },
                error => {
                    this.loading = false;
                    this.alerts.error(error);
                }
            );
    }

    selectEvento(evento: Evento) {
        this.eventosService.getAllGrupos(evento.id).subscribe(grupos => {
            this.grupos = grupos;
        });
        this.eventosStore.selectEvento(evento);
    }

    selectGrupo(grupo: Grupo) {
        this.grupo = grupo;
        if (grupo) {
            this.searchModel.saidaId = grupo.saida.id;
        }
    }

    updateUrlQuery(value: any) {
        this.router.navigate([], {
            queryParams: value,
            queryParamsHandling: 'merge'
        });
    }

    onSelectAlocacao(alocacao: Alocacao) {
        // TODO: rever como passar a returnURL
        // const returnUrl = { returnUrl: this.searchModel };
        const extras = { queryParams: this.searchModel };
        this.router.navigate([`alocacoes/${alocacao.id}`], extras);
    }

    onCreateAlocacao() {
        const extras = { queryParams: this.searchModel };
        this.router.navigate([`alocacoes/criar`], extras);
    }
}
