import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Evento } from 'src/app/core/models/eventos/evento.model';

// TODO: mudar eventos service para core módulo ou trocar esta dependencia
import { EventosService } from 'src/app/eventos/eventos.service';
import { AlertsService } from 'src/app/core/services/alerts.service';

@Injectable()
export class AlocacoesListResolver implements Resolve<Evento[]> {

    constructor(
        private router: Router,
        private eventoService: EventosService,
        private alerts: AlertsService
    ) { }

    resolve(): Observable<Evento[]> {
        return this.eventoService.getAllEventos().pipe(
            catchError(error => {
                console.log(error);
                this.alerts.warning('Não foi possível carregar os eventos');
                this.router.navigate(['']);
                return of(null);
            })
        );
    }
}
