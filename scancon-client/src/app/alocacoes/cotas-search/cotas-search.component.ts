import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { FormGroup } from '@angular/forms';

import { CotaSearch } from '../cota-search.model';
import { Evento } from 'src/app/core/models/eventos/evento.model';

@Component({
    selector: 'app-cotas-search',
    templateUrl: './cotas-search.component.html'
})
export class CotasSearchComponent implements OnInit {
    @Input()
    cotaSearch: CotaSearch;

    @Input()
    eventos: Evento[] = new Array();

    @Output('cotaSearch')
    cotaSearchEmitter = new EventEmitter();

    @Output('evento')
    eventoEmitter = new EventEmitter();

    searchForm: FormGroup;
    loading = false;

    constructor(private rxFormBuilder: RxFormBuilder) { }

    ngOnInit() {
        this.loading = true;
        this.searchForm = this.rxFormBuilder.formGroup(this.cotaSearch);

        const evento = this.eventos.filter(e => e.id == this.cotaSearch.eventoId).pop();
        if (evento) {
            this.eventoEmitter.emit(evento);
        }
        this.loading = false;
    }

    onSearch() {
        if (this.searchForm.invalid) {
            return;
        }

        const evento = this.eventos.filter(e => e.id == this.cotaSearch.eventoId).pop();
        this.eventoEmitter.emit(evento);
        this.cotaSearchEmitter.emit(this.cotaSearch);
    }

    invalid() {
        return this.searchForm.invalid;
    }
}
