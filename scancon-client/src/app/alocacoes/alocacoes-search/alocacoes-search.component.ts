import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { FormGroup } from '@angular/forms';

import { Evento } from 'src/app/core/models/eventos/evento.model';
import { AlocacaoSearch } from '../alocacao-search-model';
import { Grupo } from 'src/app/core/models/eventos/grupo.model';

@Component({
    selector: 'app-alocacoes-search',
    templateUrl: './alocacoes-search.component.html'
})
export class AlocacoesSearchComponent implements OnInit {
    @Input()
    searchModel: AlocacaoSearch;

    @Input()
    eventos: Evento[] = new Array();

    @Input()
    grupos: Grupo[] = new Array();

    @Output()
    alocaocaoSearch = new EventEmitter();

    @Output()
    evento = new EventEmitter();

    @Output()
    grupo = new EventEmitter();

    searchForm: FormGroup;

    constructor(private rxFormBuilder: RxFormBuilder) { }

    ngOnInit() {
        this.searchForm = this.rxFormBuilder.formGroup(this.searchModel);

        const selectedEvento = this.getSelectedEvento();
        if (selectedEvento) {
            this.evento.emit(selectedEvento);
        }
    }

    onSearch() {
        if (this.searchForm.invalid) {
            return;
        }

        const selectedEvento = this.getSelectedEvento();
        this.evento.emit(selectedEvento);
        this.alocaocaoSearch.emit(this.searchModel);
    }

    getSelectedEvento() {
        return this.eventos.filter(e => e.id == this.searchModel.eventoId).pop();
    }

    onSelectEvento() {
        const selectedEvento = this.getSelectedEvento();
        this.evento.emit(selectedEvento);
    }

    getSelectedGrupo() {
        return this.grupos.filter(g => g.id == this.searchModel.grupoId).pop();
    }

    onSelectGrupo() {
        const selectedGrupo = this.getSelectedGrupo();
        this.grupo.emit(selectedGrupo);
    }

    invalid() {
        return this.searchForm.invalid;
    }
}
