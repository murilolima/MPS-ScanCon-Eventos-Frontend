import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { NgxLoadingModule } from 'ngx-loading';

import { SimNaoPipe } from './sim-nao.pipe';
import { MoedaPipe } from './moeda.pipe';
import { ParticipantesListViewComponent } from './participantes-list-view/participantes-list-view.component';
import { ParticipantesSearchDialogComponent } from './participantes-search-dialog/participantes-search-dialog.component';
import { PageTitleComponent } from './page-title/page-title.component';
import { FormDirective } from './directives/form.directive';
import { ParticipantesSearchComponent } from './participantes-search/participantes-search.component';
import { ErrorComponent } from './error-component/error.component';
import { AutoFocusDirective } from './directives/autofocus.directive';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RxReactiveFormsModule,
        NgxLoadingModule,
    ],
    declarations: [
        SimNaoPipe,
        MoedaPipe,
        ParticipantesListViewComponent,
        ParticipantesSearchDialogComponent,
        ErrorComponent,
        PageTitleComponent,
        FormDirective,
        AutoFocusDirective,
        ParticipantesSearchComponent
    ],
    exports: [
        SimNaoPipe,
        MoedaPipe,
        ParticipantesListViewComponent,
        ParticipantesSearchDialogComponent,
        ErrorComponent,
        PageTitleComponent,
        AutoFocusDirective,
        FormDirective
    ]
})
export class SharedModule { }
