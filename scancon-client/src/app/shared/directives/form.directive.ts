import { Directive, HostListener, ElementRef, Renderer2 } from '@angular/core';

@Directive({
    selector: '[appForm]'
})
export class FormDirective {

    constructor(private renderer: Renderer2, private elementRef: ElementRef) { }

    @HostListener('submit', ['$event'])
    onSubmit(event: Event) {
        this.renderer.addClass(this.elementRef.nativeElement, 'was-submitted');
    }
}
