import { prop } from '@rxweb/reactive-form-validators';

export class ParticipanteSearch {
    @prop()
    nomeCpfCnpj: string;

    alocacaoId: number;
}
