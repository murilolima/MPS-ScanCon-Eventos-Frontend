import { Component, OnInit, TemplateRef, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Observable } from 'rxjs';

import { Participante } from 'src/app/core/models/participante/participante.model';
import { ParticipantesService } from 'src/app/participantes/participantes.service';
import { ParticipanteSearch } from './participante.search.model';

@Component({
    selector: 'app-participantes-search-dialog',
    templateUrl: './participantes-search-dialog.component.html'
})
export class ParticipantesSearchDialogComponent implements OnInit {

    modalRef: BsModalRef;

    participantes: Participante[];

    searchModel: ParticipanteSearch;

    @Output()
    participante = new EventEmitter();

    @Output()
    newParticipante = new EventEmitter();

    @Input()
    showTrigger: Observable<void>;

    @Input()
    closeTrigger: Observable<void>;

    @ViewChild('searchParticipantesModal', { static: false })
    private searchParticipantesModal: TemplateRef<any>;

    constructor(
        private participanteService: ParticipantesService,
        private modalService: BsModalService
    ) { }

    ngOnInit() {
        this.searchModel = new ParticipanteSearch();
        this.showTrigger.subscribe(() => this.openModal());
        this.closeTrigger.subscribe(() => this.closeModal());
    }

    openModal() {
        this.searchModel = new ParticipanteSearch();
        this.participantes = [];
        const options = Object.assign({}, { class: 'modal-lg' });
        this.modalRef = this.modalService.show(this.searchParticipantesModal, options);
    }

    closeModal() {
        this.modalRef.hide();
    }

    selectParticipante(participante: Participante) {
        this.participante.emit(participante);
    }

    searchParticipantes() {
        this.participanteService.getParticipantes(this.searchModel).subscribe(participantes => {
            this.participantes = participantes;
        });
    }

    createParticipante() {
        this.newParticipante.emit(this.searchModel);
    }
}
