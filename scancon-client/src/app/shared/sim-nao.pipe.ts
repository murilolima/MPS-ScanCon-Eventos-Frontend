import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'simnao'
})
export class SimNaoPipe implements PipeTransform {
    transform(value: boolean, ...args: any[]) {
        return value ? 'Sim' : 'Não';
    }
}
