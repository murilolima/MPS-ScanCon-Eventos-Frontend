import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Participante } from 'src/app/core/models/participante/participante.model';

@Component({
    selector: 'app-participantes-list-view',
    templateUrl: './participantes-list-view.component.html'
})
export class ParticipantesListViewComponent {

    @Input()
    participantes: Participante[];

    @Output()
    participante = new EventEmitter();

    constructor() { }

    selectParticipante(participante: Participante) {
        this.participante.emit(participante);
    }
}
