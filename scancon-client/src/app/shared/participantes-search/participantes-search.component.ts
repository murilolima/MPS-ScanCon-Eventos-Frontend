import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';

import { ParticipanteSearch } from '../participantes-search-dialog/participante.search.model';

@Component({
    selector: 'app-participantes-search',
    templateUrl: './participantes-search.component.html'
})
export class ParticipantesSearchComponent implements OnInit {

    @Input()
    searchModel: ParticipanteSearch;

    @Output()
    searchParticipantes = new EventEmitter();

    @Output()
    createParticipante = new EventEmitter();

    searchForm: FormGroup;
    loading = false;

    constructor(private rxFormBuilder: RxFormBuilder) { }

    ngOnInit() {
        this.searchForm = this.rxFormBuilder.formGroup(this.searchModel);
    }

    onSearch() {
        if (this.searchForm.invalid) {
            return;
        }
        this.searchParticipantes.emit(this.searchModel);
    }

    onCreate() {
        this.createParticipante.emit(this.searchModel);
    }

}
