import { Component, Input, SkipSelf, Host } from '@angular/core';
import { FormGroupDirective, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-error-component',
  templateUrl: './error.component.html'
})
export class ErrorComponent {

  @Input() controlName: string;
  control: FormControl;

  constructor(@Host() @SkipSelf() private form: FormGroupDirective) {
  }

  getErros() {
    const control = this.form.control.controls[this.controlName];
    return control ? control['errorMessages'] : [];
  }
}
