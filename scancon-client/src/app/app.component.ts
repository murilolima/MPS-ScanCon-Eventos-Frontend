import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { TitleService } from './core/services/title.service';
import { filter, map } from 'rxjs/operators';
import { ReactiveFormConfig } from '@rxweb/reactive-form-validators';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
    constructor(private router: Router, private activatedRoute: ActivatedRoute, private titleService: TitleService) {}

    ngOnInit() {
        const appTitle = this.titleService.getTitle();
        this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                map(() => {
                    let child = this.activatedRoute.firstChild;
                    while (child.firstChild) {
                        child = child.firstChild;
                    }
                    if (child.snapshot.data.title) {
                        return child.snapshot.data.title;
                    }
                    return appTitle;
                })
            )
            .subscribe((title: string) => {
                this.titleService.setTitle(title);
            });

        ReactiveFormConfig.set({
            validationMessage: {
                required: 'Campo Obrigatório.',
                minLength: 'Minímo de caracteres não foi atingido',
                maxLength: 'Máximo de caracteres atingido.',
                numeric: 'Somente números'
            }
        });
    }
}
