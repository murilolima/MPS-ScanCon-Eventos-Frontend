# Step 1: Build the app in image 'builder'
FROM node:12.6.0-alpine AS builder

ARG http_proxy
ARG https_proxy
ARG configuration

WORKDIR /scancon-client
COPY /scancon-client .

RUN npm config set proxy $http_proxy
RUN npm config set https-proxy $https_proxy

RUN npm ci && npm run build-prod --configuration=$configuration

# Step 2: Use build output from 'builder'
FROM nginx:stable-alpine

COPY nginx.conf /etc/nginx/nginx.conf

# Instando o CURL para podemos consultar o endpoint de healthcheck
RUN apk --no-cache add curl

WORKDIR /usr/share/nginx/html
COPY --from=builder /scancon-client/dist/scancon-frontend/ .

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD [ "curl", "http://localhost/health" ]

ENV BACKEND_API_URL='http://opala.mps.interno:5000/'

#TODO: mover para script bash separado "run.sh"
ENTRYPOINT ["/bin/sh",  "-c",  "envsubst < assets/settings/settings.template.json > assets/settings/settings.json && exec nginx -g 'daemon off;'"]
