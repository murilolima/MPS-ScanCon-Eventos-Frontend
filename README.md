# Frontend - Eventos

Repositório com o Frontend para o sistema de eventos da ScanCon.

## Status

### Master / Stable

[![Build Status](https://logan.mps.interno/buildStatus/icon?job=ScanCon+Eventos+-+Frontend%2Fmaster)](https://logan.mps.interno/job/ScanCon%20Eventos%20-%20Frontend/job/master/)

### Develop

[![Build Status](https://logan.mps.interno/buildStatus/icon?job=ScanCon+Eventos+-+Frontend%2Fdevelop)](https://logan.mps.interno/job/ScanCon%20Eventos%20-%20Frontend/job/develop/)

## Responsável

O colaborador responsável por este repositório é: [Murilo Lima](https://intranet.mps.com.br/Colaboradores/Mostrar/717).

Em caso de dúvidas ou sugestões favor entrar em contato via jabber (_murilo_) ou e-mail (_murilo.lima@mps.com.br_).

## Tecnologias

As seguintes tecnologias são utilizadas neste projeto:

+ **NodeJS**: Para controle de dependências via `npm` e hospedar o `angular`
+ **Angular-CLI**: Para adicionar os elementos do `Angular` ao projeto
+ **Jenkins**: Através do `Jenkinsfile` realizará build e publish da solução
+ **Docker**: Através do `Dockerfile` e `docker compose` realizará o publish de nossa solução

## Projetos

WIP

## Compilando e Testando

### Pré Requisitos

É necessário possuir uma versão LTS ou a mais recente do [NodeJS](https://nodejs.org/en/).

Uma vez instalado o `NodeJS` instale as ferramentas de CLI do Angular, através do comando `npm install -g @angular/cli`.

### Ambiente de Desenvolvimento

Utilize o comnado `ng install`, no diretório `scancon-client` para instalar as dependências do projeto.

Utilize o comando `ng serve` para iniciar um ambiente de desenvolvimento em `http://localhost:4200`.

Utilizando esta instância de desenvolvimento o Angular subirá um servidor com suporte a `Hot Reload`.

### Compilando

Utilize o comando `ng build` para compilar o projeto. Isso gerará um novo diretório `dist/`.

Para ambientes de produção adicione a flag `--prod`.

### Testes Unitários

Utilize o comando `ng test` para executar os testes unitários utilizando o provider `Karma`.

### Testes Ponta-a-Ponta

Utilize o comando `ng e2e` para executar os testes de integração utilizando o provider `Protractor`.

## Contribuindo com o Repositório

Abaixo detalhamos as práticas e funções dos membros da organização neste repositório.

### Responsáveis

Os responsáveis por revisar, aceitar e realizar merge das Pull Requests são os membros do grupo [Analistas](https://git.mps.com.br/org/scania/teams/analistas).

### Git Flow

A documentação completa do Git Flow está disponível no [site oficial do git](https://git-scm.com/docs/gitworkflows) .

Abaixo apresentamos uma versão resumida.

#### Master

O branch `master` contem a versão de produção do código. Sempre receberá merges dos branchs `release/*` e, eventualmente, dos branches `hotfix/*`.

O Merge deverá ser feito pelo comando `git flow` por um usuário com permissão de push no `master`.

Este branch é **protegido** por padrão. O que significa que nenhum usuário fora da whitelist de merge poderá realizar pushes ou mergear elementos neste branch.

#### Releases

Os branches de releases `release/*` são branches temporários para o processo de release de novas versões. Eles devem partir do branch `develop` e serem mergeados no branch `master`, junto com uma tag de release.

As tags para este projeto devem utilizar o [semantic versioning 2.0](https://semver.org/), respeitando o padrão: `<major>.<minor>.<patch>` e com o sufixo adicional de pre-releases `-alpha.x|-beta.x|-rc.x`.

Tags válidas são, por exemplo:

+ 0.1.0-alpha.1
+ 1.1.0-rc.1
+ 1.3.0-alpha.1
+ 1.3.1-alpha.1

#### Develop

O branch `develop` contem a versão de desenvolvimento do código. Será a partir dele que serão realizadas as `release/*` e `feature/*`.

Este branch é protegido e apenas membros do grupo [Analistas](https://git.mps.com.br/org/scania/teams/analistas) podem realizar `push`, `merge`, revisar e aceitar `Pull Requests`.

#### Features

Os branches `feature/*` são branches individuais de desenvolvimento. Cada branch de feature deve depender **apenas** do `develop` e representar uma **funcionalidade** a ser adicionada ao código.

Estes branches são livres para serem criados por quaisquer usuários desta organização.

No fim de seu ciclo estes branches serão mergeados com o `develop` via `Pull Requests`.

#### Hotfixes

Os branches `hotfix/*` são branches de correções de alta importância. Cada branch de hotfix deve depender **apenas** do `develop`.

Estes branches são livres para serem criados por quaisquer usuários desta organização.

No fim de seu ciclo estes branches serão mergeados com o `master` por um dos membros com permissão de `merge` no `master`.

### Ferramentas recomendadas

Recomendamos a utilização das seguintes ferramentas para visualizar e trabalhar com este repositório:

+ [Git Fork](https://git-fork.com/): Um client gui para o Git que possui suporte ao Git-Flow
+ [Visual Studio Code](https://code.visualstudio.com/): Um ambiente de desenvolvimento leve e extensível


### Guia de estilo do angular e angular CLI

Recomendamos a utilização do angular cli, juntamente com o [style guide](https://angular.io/guide/styleguide) do angular.
Este guia traz alguma príncipios, entre eles: estrutura do projeto, nomenclatura de nomes de models, nomenclatura de serviços, separação de features em submódulos, entre outros.

Para adicionar uma nova feature ao projeto, seguir esta [documentação (lazy loading feature modules))](https://angular.io/guide/lazy-loading-ngmodules)