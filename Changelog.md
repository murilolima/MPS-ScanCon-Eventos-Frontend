# Changelog
Todas as alterações importantes para este projeto serão documentadas neste arquivo.

## [Não lançado]

- #145 Ajustar menu quando recarrega a página
- #167 Formatar exibição de CPF/CNPJ
- #122 Exibir apenas vigentes para alocação
- #149 Corrigir disparo de validação dos formulários
- #160 Adicionar restrição de unicidade nos campos Passaporte, RG e CPF do Participante
- #111 Adicionar restrições de unicidade no Tipo de Evento e Evento
- #144 Ajustar cadastro de entrada de decimal

## [0.4.0] - 01/10/2019
### Adicionado
- Cadastrar Participantes em Alocação Sem Cota
- Criar alocação sem Grupo e Cota
- Criar novo participante a partir de da tela de alocação
- Filtrar por nome e CPF na listagem de participantes

### Melhorado
- Cadastro de Participantes -Inclusão dos campos Estado e Número
- Cadastro de Participantes - Voltar para listagem após salvar
- Cadastro de Participantes - Revisão de campos obrigatórios e opcionais
- Cadastro de Participantes - Formatações
- Cadastro de Participantes - Habilitados campos RG, CPF e Data de Nascimento
- Cadastro de Participantes - Adiciona Tipo de Participante
- Cadastro de Participantes - Ajuste do campo visto
- Não permitir excluir um participante que já esteja vinculado com alguma Alocação.
- Não permitir alocar mais de uma vez um participante
- Não exibir dialog de confirmação
- Não permitir voltar as listagens caso Create retorne erro
- Nas telas de edição, voltar para a tela de listagem.
- Padronização botões de listagem
- Padronização click botão "salvar", agora botão fica habilitado sempre
- Alterado saída para opcional na tela de consulta de alocações
- Ajuste layout dos títulos das páginas
- Ajuste layout de validação dos campos
- Ajuste logo
- Posicinar foco dos formulários no primeiro campo
- Cadastro de eventos: Permitir que o usuário possa editar o campo "Tipo Evento", selecionando uma nova opção
- Não deixar excluir um evento para o qual já tenha alguma alocação feita.
- Desativar campos que não podem ser editados
- Mensagem de alterações pendentes aparece, mesmo após salvar
- Habilitado filtro grupo, cota e "CPF/CNPJ" na ciração da alocação
- Retirar botão desfazer


## [0.3.1] - 18/09/2019
### Adicionado
- Cadastro de Participantes (Listar, Editar e Excluir)
- Criar Alocação com Cota
- Listar Alocações
- Detalhar Alocações
- Vincular participante em Alocação
- Validação dos campos dos formulários (vermelho e verde)


### Melhorado
- Melhorias nos scripts docker (implantação)
- Adicionado configuração do frontend via variável de ambiente
- Melhorias nos campos de data (datepicker)
- Padronização dos botões de ação, voltar e das listagens
- Correções gerais de bugs

## [0.2.0] - 03/09/2019
### Adicionado

- Framework Inicial
- Cadastro de eventos
- Parâmetros:
    - Tipos de Evento
    - Tipos de Produto
    - Tipos de Participante
    - Tipos de Acomodação
    - Tipos de Comprovante
- Processo de entrega (utilizando Git/Docker)